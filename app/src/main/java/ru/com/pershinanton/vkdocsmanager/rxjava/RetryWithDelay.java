package ru.com.pershinanton.vkdocsmanager.rxjava;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

public class RetryWithDelay implements Function<Flowable<? extends Throwable>, Flowable<?>> {
    public static final int UNLIMITED = -1;
    private final int maxRetries;
    private final int retryDelayMillis;
    private final Listener listener;
    private int retryCount;

    public RetryWithDelay(final int maxRetries, final int retryDelayMillis, Listener listener) {
        this.maxRetries = maxRetries;
        this.retryDelayMillis = retryDelayMillis;
        this.retryCount = 0;
        this.listener = listener;
    }

    public RetryWithDelay(final int retryDelayMillis, Listener listener) {
        this.retryDelayMillis = retryDelayMillis;
        this.maxRetries = UNLIMITED;
        this.retryCount = 0;
        this.listener = listener;
    }

    @Override
    public Flowable<?> apply(final Flowable<? extends Throwable> attempts) {
        return attempts
                .flatMap((Function<Throwable, Flowable<?>>) throwable -> {
                    if ((maxRetries == 0) || (listener != null && !listener.onRequestRetryNetwork(throwable)))
                        return Flowable.error(throwable);
                    if ((maxRetries == UNLIMITED) || ++retryCount < maxRetries) {
                        // When this Observable calls onNext, the original
                        // Observable will be retried (i.e. re-subscribed).
                        return Flowable.timer(retryDelayMillis,
                                TimeUnit.MILLISECONDS);
                    }
                    return Flowable.error(throwable);
                });
    }

    public interface Listener {
        boolean onRequestRetryNetwork(Throwable throwable);
    }

}