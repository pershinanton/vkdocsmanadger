package ru.com.pershinanton.vkdocsmanager.ui.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import ru.com.pershinanton.vkdocsmanager.R;
import com.fondesa.lyra.Lyra;
import com.hwangjr.rxbus.RxBus;

import butterknife.ButterKnife;
import butterknife.Unbinder;


public abstract class BaseFragment extends MvpAppCompatFragment {
    protected static final String INSTANTIATE_OBJECT = "INSTANTIATE_OBJECT";
    protected static final String MODEL = "MODEL";
    protected static final String ID = "ID";

    private final boolean useButterKnifeBinder;
    private final boolean useRxBus;
    private final Orientation orientation;

    private boolean isFragmentOnPause;
    private Unbinder unbinder;
    private View view;


    public BaseFragment(boolean useButterKnifeBinder, boolean useRxBus, Orientation orientation) {
        this.useButterKnifeBinder = useButterKnifeBinder;
        this.useRxBus = useRxBus;
        this.orientation = orientation;
    }

    public BaseFragment() {
        this.useButterKnifeBinder = true;
        this.useRxBus = true;
        this.orientation = Orientation.NONE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Lyra.instance().restoreState(this, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Lyra.instance().saveState(this, outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (orientation != null)
            switch (orientation) {
                case NONE:
                    break;
                case PORTRAIT:
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
                case LANDSCAPE:
                    getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    break;
                case PHONE_PORTRAIT_TABLET_LANDSCAPE:
                    if (getResources().getBoolean(R.bool.isTablet))
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    else
                        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    break;
            }
        isFragmentOnPause = false;
        view = inflater.inflate(onInflateLayout(), container, false);
        if (useButterKnifeBinder) unbinder = ButterKnife.bind(this, view);
        if (useRxBus)
            RxBus.get().register(this);
        onViewInflated(view, savedInstanceState);
        return view;
    }

    public boolean isFragmentOnPause() {
        return isFragmentOnPause;
    }

    @Override
    public void onPause() {
        super.onPause();
        isFragmentOnPause = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        isFragmentOnPause = false;
    }

    @Override
    public void onDestroyView() {
        if (useButterKnifeBinder)
            releaseButterKnife();
        if (useRxBus)
            releaseRxBus();
        super.onDestroyView();
    }

    private void releaseButterKnife() {
        if (unbinder != null) {

            unbinder.unbind();
            unbinder = null;
        }
    }

    private void releaseRxBus() {
        try {
            RxBus.get().unregister(this);
        } catch (Exception e) {
        }
    }

    public abstract void onViewInflated(View view, @Nullable Bundle savedInstanceState);

    public abstract @LayoutRes
    int onInflateLayout();

    public enum Orientation {
        NONE, PORTRAIT, LANDSCAPE, PHONE_PORTRAIT_TABLET_LANDSCAPE
    }
}