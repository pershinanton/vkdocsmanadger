package ru.com.pershinanton.vkdocsmanager.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VkDocsModel implements Parcelable {
    public static final int TYPE_PDF = 1;
    public static final int TYPE_ARHIVE = 2;
    public static final int TYPE_IMAGE = 4;
    public static final int TYPE_AUDIO = 5;
    public static final int TYPE_VIDEO = 6;
    public static final int TYPE_BOOKS = 7;
    public static final int TYPE_UNKNOW = 8;


    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("size")
    @Expose
    private long size;
    @SerializedName("ext")
    @Expose
    private String ext;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("type")
    @Expose
    private int type;

    public long getDate() {
        return date;
    }

    @SerializedName("date")
    @Expose
    private long date;
    @SerializedName("preview")
    @Expose
    private PreviewModel previewModel;

    public static final Creator<VkDocsModel> CREATOR = new Creator<VkDocsModel>() {
        @Override
        public VkDocsModel createFromParcel(Parcel in) {
            return new VkDocsModel(in);
        }

        @Override
        public VkDocsModel[] newArray(int size) {
            return new VkDocsModel[size];
        }
    };

    public PreviewModel getPreviewModel() {
        return previewModel;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public long getSize() {
        return size;
    }

    public String getExt() {
        return ext;
    }

    public String getUrl() {
        return url;
    }

    public int getType() {
        return type;
    }

    protected VkDocsModel(Parcel in) {
        type = in.readInt();
        url = in.readString();
        title = in.readString();
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeString(url);
        dest.writeString(title);
    }
}