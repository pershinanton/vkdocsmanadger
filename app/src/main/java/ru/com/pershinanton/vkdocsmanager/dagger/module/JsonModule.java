package ru.com.pershinanton.vkdocsmanager.dagger.module;

import ru.com.pershinanton.vkdocsmanager.gson.DeserializationExclusionStrategy;
import ru.com.pershinanton.vkdocsmanager.gson.SerializationExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class JsonModule {
    @Provides
    @Singleton
    Gson gson() {
        return new GsonBuilder()
//                                        .registerTypeAdapter(NewsPartModel.class, new CartoonDetailedContentModelDeserializer())
                .addDeserializationExclusionStrategy(new DeserializationExclusionStrategy())
                .addSerializationExclusionStrategy(new SerializationExclusionStrategy())
                .create();
    }

}