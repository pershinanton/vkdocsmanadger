package ru.com.pershinanton.vkdocsmanager.main;

import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpView;

public interface FragmentImageViewMvpView extends BaseNetworkMvpView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataError(String message);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataLoading(Integer percent);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onLoadingFinished(String finished);
    @StateStrategyType(OneExecutionStateStrategy.class)
    void onLoadingError(String error);

}
