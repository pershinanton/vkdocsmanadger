package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;
import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;

public class VkFriendsGetResponse {
    @SerializedName("count")
    private int count;
    @SerializedName("items")
    private List<VkFriendsModel> items;

    public int getCount() {
        return count;
    }

    public List<VkFriendsModel> getItems() {
        return items;
    }
}
