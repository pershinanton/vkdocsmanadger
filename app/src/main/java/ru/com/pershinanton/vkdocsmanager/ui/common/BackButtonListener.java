package ru.com.pershinanton.vkdocsmanager.ui.common;

public interface BackButtonListener {
    boolean onBackPressed();
}
