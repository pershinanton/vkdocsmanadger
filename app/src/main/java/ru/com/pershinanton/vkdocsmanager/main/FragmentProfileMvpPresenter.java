package ru.com.pershinanton.vkdocsmanager.main;

import com.arellomobile.mvp.InjectViewState;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.LogoutController;
import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.base.UnauthorizedIOException;
import ru.com.pershinanton.vkdocsmanager.model.VkUserModel;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.retrofit.ApiClient;
import ru.com.pershinanton.vkdocsmanager.retrofit.exception.ApiException;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkResponse;
import ru.com.pershinanton.vkdocsmanager.rxjava.RetryWithDelay;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class FragmentProfileMvpPresenter extends BaseNetworkMvpPresenter<FragmentProfileMvpView> implements BackButtonListener, RetryWithDelay.Listener {// в этом классе мы получаем данные из других мест

    @Inject
    ApiClient apiClient;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    LogoutController logoutController;
    private Router router;
    private Disposable disposable;

    public FragmentProfileMvpPresenter(Router router) {
        super(false);
        this.router = router;
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        load();
    }

    public void load() {
        disposable = Flowable.just(true)
//                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
                    .retry(throwable -> onRequestRetryNetwork(throwable))
                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {
                    Response<VkResponse<List<VkUserModel>>> response;
                    try {                                           // response ответ с сервера по обращению
                        response = apiClient.usersGet(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken(), "photo_400_orig").execute();
//                      // https://api.vk.com/method/docs.get?v=5.92&lang=ru&access_token=ab22e38f8aa3a11cc87f7c49b8f49a683019f6fced677fc2588061e55db2833cb2739f338da3079ac9f0d&count=5&offset=0&type=0&owner_id=4212530
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new ApiException();
                    if (response.code() != 200) throw new ApiException();
                    if (response.errorBody() != null) throw new ApiException();
                    if (response.body() == null) throw new ApiException();
                    VkResponse<List<VkUserModel>> apiResponse = response.body();

                    if (apiResponse.getError() != null && apiResponse.getError().getErrorCode() > 0) {
                        if (apiResponse.getError().getErrorCode() == 5) {
                            logoutController.unauthorized();
                            throw new UnauthorizedIOException();
                        } else {
                            if (apiResponse != null && apiResponse.getError().getErrorMsg() != null && apiResponse.getError().getErrorMsg().length() > 0)
                                throw new ApiException(apiResponse.getError().getErrorMsg());
                            else
                                throw new ApiException();
                        }
                    }

                    List<VkUserModel> result = apiResponse.response;

                    if (result == null)
                        throw new ApiException();

                    return result;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    getViewState().onDataLoaded(list);
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    protected void dispose() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            disposable = null;
        }
    }

    @Override
    public void onDestroy() {
        dispose();

        super.onDestroy();
        router = null;
    }

    @Override
    public boolean onBackPressed() {
        if (router != null) router.exit();
        return true;
    }

}