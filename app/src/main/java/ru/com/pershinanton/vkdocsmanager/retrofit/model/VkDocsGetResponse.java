package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;

public class VkDocsGetResponse {
    @SerializedName("count")
    private long count;
    @SerializedName("items")
    private List<VkDocsModel> items;

    public long getCount() {
        return count;
    }

    public List<VkDocsModel> getItems() {
        return items;
    }
}
