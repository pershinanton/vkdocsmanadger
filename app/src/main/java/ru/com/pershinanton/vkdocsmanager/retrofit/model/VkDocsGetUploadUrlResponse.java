package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;


public class VkDocsGetUploadUrlResponse {
    @SerializedName("upload_url")
    private String uploadUrl;

    public String getUploadUrl() {
        return uploadUrl;
    }
}
