package ru.com.pershinanton.vkdocsmanager.main.base;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;
import com.hwangjr.rxbus.RxBus;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpRetryException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import ru.com.pershinanton.vkdocsmanager.retrofit.exception.ApiException;

public abstract class BaseMvpPresenter<T extends MvpView> extends MvpPresenter<T> {



    public BaseMvpPresenter() {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            RxBus.get().unregister(this);
        } catch (Exception e) {
        }

    }



}