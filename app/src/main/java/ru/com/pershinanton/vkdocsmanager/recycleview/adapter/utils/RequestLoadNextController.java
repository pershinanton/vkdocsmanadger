package ru.com.pershinanton.vkdocsmanager.recycleview.adapter.utils;

import java.util.EventListener;

public class RequestLoadNextController {
    public static final int NO_ITEM = -1;
    private int startLoadBeforeItemsCount = 0;
    private int maxVisibleItem = NO_ITEM;

    private OnRequestLoadNextListener onRequestLoadNextListener;
    private boolean enabled;

    public RequestLoadNextController() {
    }

    public RequestLoadNextController setRequestLoadNextListener(int startLoadBeforeCount, OnRequestLoadNextListener listener) {
        this.onRequestLoadNextListener = listener;
        this.startLoadBeforeItemsCount = startLoadBeforeCount;
        resetNeedLoadNextCounter();
        return this;
    }

    public void checkIsNeedLoadNext(int count, int position) {
        if (enabled)
            if (maxVisibleItem < position) {
                maxVisibleItem = position;
                if (maxVisibleItem >= count - startLoadBeforeItemsCount) {
                    maxVisibleItem = count;
                    if (onRequestLoadNextListener != null)
                        onRequestLoadNextListener.onRequestLoad();
                }
            }
    }

    public void resetNeedLoadNextCounter() {
        maxVisibleItem = -1;
    }

    public RequestLoadNextController setStartLoadBeforeItemsCount(int startLoadBeforeItemsCount) {
        this.startLoadBeforeItemsCount = startLoadBeforeItemsCount;
        return this;
    }

    public int getMaxVisibleItem() {
        return maxVisibleItem;
    }

    public RequestLoadNextController setMaxVisibleItem(int maxVisibleItem) {
        this.maxVisibleItem = maxVisibleItem;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public interface OnRequestLoadNextListener extends EventListener {
        void onRequestLoad();
    }

}