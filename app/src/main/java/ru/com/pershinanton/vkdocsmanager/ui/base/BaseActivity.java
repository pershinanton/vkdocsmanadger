package ru.com.pershinanton.vkdocsmanager.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import com.hwangjr.rxbus.RxBus;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private Unbinder unbinder;

    public static BaseActivity get(Context context) {
        return ((BaseActivity) context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(onInflateLayout());
        onViewInflated(savedInstanceState);

    }

    public void initButterKnife() {
        unbinder = ButterKnife.bind(this);
    }

    private void releaseButterKnife() {
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }

    public void initRxBus() {
        RxBus.get().register(this);
    }

    private void releaseRxBus() {
        try {
            RxBus.get().unregister(this);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        releaseButterKnife();
        releaseRxBus();
        super.onDestroy();
    }

    public abstract @LayoutRes
    int onInflateLayout();

    protected abstract void onViewInflated(Bundle savedInstanceState);

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}