package ru.com.pershinanton.vkdocsmanager.main;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.BusDownload;
import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.retrofit.DownloadFileApiClient;
import ru.com.pershinanton.vkdocsmanager.rxjava.RetryWithDelay;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

import static com.google.android.exoplayer2.ExoPlayerLibraryInfo.TAG;

@InjectViewState
public class FragmentImageViewMvpPresenter extends BaseNetworkMvpPresenter<FragmentImageViewMvpView> implements BackButtonListener, RetryWithDelay.Listener {// в этом классе мы получаем данные из других мест

    @Subscribe(
            thread = EventThread.MAIN_THREAD,
            tags = {
                    @Tag(BusDownload.DOWNLOAD_PERCENT)
            }
    )
    public void downloading(Integer percent) {
        getViewState().onDataLoading(percent);

    }

    @Subscribe(
            thread = EventThread.MAIN_THREAD,
            tags = {
                    @Tag(BusDownload.DOWNLOAD_ERROR)
            }
    )
    public void eror(boolean error) {
        getViewState().onLoadingError("Ошибка загрузки");
    }


    private Router router;
    private Disposable disposable;

    @Inject
    DownloadFileApiClient downloadFileApiClient;
    boolean end = true;

    public FragmentImageViewMvpPresenter(Router router) {
        super(true);
        this.router = router;
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

    }

    public void download(String fileUrl, String title, String pathName) {

        Call<ResponseBody> call = downloadFileApiClient.downloadFileWithDynamicUrlSync(fileUrl);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "server contacted and has file");

                    boolean writtenToDisk = writeResponseBodyToDisk(response.body(), title, pathName);

                    Log.d(TAG, "file download was a success? " + writtenToDisk);
                    if (writtenToDisk == true) {
                        getViewState().onLoadingFinished("Загрузка завершена");
                    }
                } else {
                    Log.d(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "error");
            }
        });
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String title, String pathName) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(pathName + File.separator + title);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;

                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    protected void dispose() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            disposable = null;
        }
    }


    @Override
    public void onDestroy() {
        dispose();
        super.onDestroy();
        router = null;
    }

    @Override
    public boolean onBackPressed() {
        if (router != null) router.exit();
        return true;
    }


}