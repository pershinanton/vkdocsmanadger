package ru.com.pershinanton.vkdocsmanager.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class SerializationExclusionStrategy implements ExclusionStrategy {
    public boolean shouldSkipClass(Class<?> clazz) {
        return clazz.getAnnotation(Exclude.class) != null || clazz.getAnnotation(ExcludeSerization.class) != null;
    }

    public boolean shouldSkipField(FieldAttributes f) {
        return f.getAnnotation(Exclude.class) != null || f.getAnnotation(ExcludeSerization.class) != null;
    }
}