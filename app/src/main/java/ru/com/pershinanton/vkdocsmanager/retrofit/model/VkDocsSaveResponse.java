package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;


public class VkDocsSaveResponse {
    @SerializedName("type")
    private String type;

    @SerializedName("doc")
    private VkDocsModel vkDocsModel;

    public String getType() {
        return type;
    }

    public VkDocsModel getVkDocsModel() {
        return vkDocsModel;
    }
}
