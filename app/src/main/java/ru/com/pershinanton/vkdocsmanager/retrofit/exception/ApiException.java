package ru.com.pershinanton.vkdocsmanager.retrofit.exception;

public class ApiException extends Exception {
    public ApiException(String message) {
        super(message);
    }

    public ApiException() {
        super();
    }

}
