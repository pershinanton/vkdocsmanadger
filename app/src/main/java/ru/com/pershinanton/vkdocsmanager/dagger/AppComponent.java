package ru.com.pershinanton.vkdocsmanager.dagger;


import ru.com.pershinanton.vkdocsmanager.dagger.module.ApplicationModule;
import ru.com.pershinanton.vkdocsmanager.dagger.module.JsonModule;
import ru.com.pershinanton.vkdocsmanager.dagger.module.NavigationModule;
import ru.com.pershinanton.vkdocsmanager.dagger.module.NetworkModule;
import ru.com.pershinanton.vkdocsmanager.lyra.GsonCollectionStateCoder;
import ru.com.pershinanton.vkdocsmanager.lyra.GsonHashMapStateCoder;
import ru.com.pershinanton.vkdocsmanager.lyra.GsonObjectStateCoder;

import ru.com.pershinanton.vkdocsmanager.main.FragmentImageViewMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.FragmentProfileMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.FragmentWallMvpPresenter;

import ru.com.pershinanton.vkdocsmanager.ui.main.FragmentImageView;
import ru.com.pershinanton.vkdocsmanager.ui.main.FragmentLogin;
import ru.com.pershinanton.vkdocsmanager.ui.main.FragmentProfile;
import ru.com.pershinanton.vkdocsmanager.ui.main.FragmentSettings;
import ru.com.pershinanton.vkdocsmanager.ui.main.FragmentWall;
import ru.com.pershinanton.vkdocsmanager.ui.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NavigationModule.class,
        NetworkModule.class,
        JsonModule.class
})
public interface AppComponent {
    void inject(MainActivity activity);

    void inject(FragmentWall fragment);

    void inject(FragmentLogin fragmentLogin);

    void inject(FragmentImageView fragmentImageView);

    void inject(FragmentProfile fragmentProfile);

//    void inject(FragmentSendMessage fragmentSendMessage);

    void inject(FragmentSettings fragmentSettings);

//    void inject(FragmentSearchMessage fragmentSearchMessage);

    void inject(GsonObjectStateCoder gsonObjectStateCoder);

    void inject(GsonHashMapStateCoder gsonHashMapStateCoder);

    void inject(GsonCollectionStateCoder gsonCollectionStateCoder);

    void inject(FragmentWallMvpPresenter fragmentWallMvpPresenter);

    void inject(FragmentImageViewMvpPresenter fragmentImageViewMvpPresenter);

    void inject(FragmentProfileMvpPresenter fragmentProfileMvpPresenter);

//    void inject(FragmentMessageMvpPresenter fragmentMessageMvpPresenter);
//
//    void inject(FragmentSearchMvpPresenter fragmentSearchMvpPresenter);
}


