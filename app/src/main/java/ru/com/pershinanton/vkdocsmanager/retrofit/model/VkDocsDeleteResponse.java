package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;


public class VkDocsDeleteResponse {
    @SerializedName("response")
    private String response;

    public String getResponse() {
        return response;
    }
}
