package ru.com.pershinanton.vkdocsmanager.recycleview.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.viewholder.FriendsWallViewHolder;

public class VkFriendsWallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final List<VkFriendsModel> models = new ArrayList<>();
    private Listener listener;
    private LayoutInflater inflater;


    public VkFriendsWallAdapter(Context context, List<VkFriendsModel> models, Listener listener) {
        if (models != null)
            this.models.addAll(models);
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FriendsWallViewHolder(inflater.inflate(R.layout.vk_fiends_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof FriendsWallViewHolder) {
            FriendsWallViewHolder vh = (FriendsWallViewHolder) viewHolder;

        }

        viewHolder.itemView.setOnClickListener(v -> {
            if (listener != null)
                listener.onClickWall(models.get(position));

        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public interface Listener {
        void onClickWall(VkFriendsModel vkFriendsModel);
    }

}
