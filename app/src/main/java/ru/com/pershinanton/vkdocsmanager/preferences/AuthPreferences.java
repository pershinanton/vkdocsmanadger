package ru.com.pershinanton.vkdocsmanager.preferences;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AuthPreferences {

    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String USER_ID = "USER_ID";
    public static final String FILE_PATH = "FILE_PATH";
    private static final String PREFS_FILENAME = AuthPreferences.class.getSimpleName();

    private final SharedPreferences prefs;
    private String accessToken;
    private String userid;
    private String filePath;

    @Inject
    public AuthPreferences(Application application) {
        prefs = application.getSharedPreferences(PREFS_FILENAME,
                Context.MODE_PRIVATE);
    }

    public String getAccessToken() {
        if (accessToken == null)
            accessToken = prefs.getString(ACCESS_TOKEN, null);
        return accessToken;
    }

    public String getUserId() {
        if (userid == null)
            userid = prefs.getString(USER_ID, null);
        return userid;
    }

    public String getFilePath() {
        if (filePath == null)
            filePath = prefs.getString(FILE_PATH, null);
        return filePath;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(ACCESS_TOKEN, accessToken);
        edit.commit();
    }

    public void setUserId(String userid) {
        this.userid = userid;
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(USER_ID, userid);
        edit.commit();
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(FILE_PATH, filePath);
        edit.commit();
    }

    public void clear() {
        accessToken = null;
        prefs.edit().clear().commit();
    }

    public boolean isSigned() {
        if (getAccessToken() != null && getAccessToken().length() > 0)
            return true;
        return false;
    }
}