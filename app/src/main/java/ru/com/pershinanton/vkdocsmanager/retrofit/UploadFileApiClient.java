package ru.com.pershinanton.vkdocsmanager.retrofit;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsUploadFileResponse;

public interface UploadFileApiClient {
    @Multipart
    @POST
    Call<VkDocsUploadFileResponse> uploadFile(@Url String url, @Part MultipartBody.Part file);
}