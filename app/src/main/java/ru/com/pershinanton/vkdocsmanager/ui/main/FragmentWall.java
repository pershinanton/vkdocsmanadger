package ru.com.pershinanton.vkdocsmanager.ui.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.developer.filepicker.controller.DialogSelectionListener;
import com.developer.filepicker.model.DialogConfigs;
import com.developer.filepicker.model.DialogProperties;
import com.developer.filepicker.view.FilePickerDialog;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.Screens;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.main.FragmentWallMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.FragmentWallMvpView;
import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.VkWallPaginationAdapter;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.utils.RequestLoadNextController;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsSaveResponse;
import ru.com.pershinanton.vkdocsmanager.ui.base.BaseFragment;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

public class FragmentWall extends BaseFragment implements FragmentWallMvpView, BackButtonListener {

    @Inject
    Toaster toaster;
    @Inject
    Router router;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @InjectPresenter
    FragmentWallMvpPresenter presenter;
    @Inject
    AuthPreferences authPreferences;
    AlertDialog.Builder ad;
    MainActivity activity;
    private VkWallPaginationAdapter adapter;
    String button1String = "Да";
    String button2String = "Нет";

    public FragmentWall() {
        super(true, false, Orientation.NONE);
        setHasOptionsMenu(true);
    }

    public static FragmentWall getNewInstance(/*InstantiateObject instantiateObject*/) {
        FragmentWall fragment = new FragmentWall();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @ProvidePresenter
    public FragmentWallMvpPresenter createPresenter() {
        return new FragmentWallMvpPresenter(router);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.wall_docs_toolbar, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add) {
            DialogProperties properties = new DialogProperties();
            properties.selection_mode = DialogConfigs.SINGLE_MODE;
            properties.selection_type = DialogConfigs.FILE_SELECT;
            properties.root = new File(DialogConfigs.DEFAULT_DIR);
            properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
            properties.offset = new File(DialogConfigs.DEFAULT_DIR);
            properties.extensions = null;
            FilePickerDialog dialog = new FilePickerDialog(activity, properties);
            dialog.setTitle("Выберите документ");
            dialog.setDialogSelectionListener(new DialogSelectionListener() {
                @Override
                public void onSelectedFilePaths(String[] files) {
                    if (files != null && files.length > 0) {
                        String filePath = files[0];
                        File file = new File(filePath);
                        String strFileName = file.getName();
                        ad = new AlertDialog.Builder(activity);
                        final EditText input = new EditText(activity);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT);
                        input.setLayoutParams(lp);
                        input.setText(strFileName);
                        ad.setTitle("Переименовать?");
                        ad.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int arg1) {
                                presenter.firstUpload(filePath, input.getText().toString());
                                dialog.cancel();
                            }
                        });
                        ad.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int arg1) {
                                presenter.firstUpload(filePath, strFileName);
                                dialog.cancel();
                            }
                        });
                        ad.setView(input);
                        ad.show();
                    }
                }
            });

            dialog.show();
        }
        return true;
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        activity.showBottomNavigationView();
        activity.getSupportActionBar().setTitle("Документы");
        activity.getSupportActionBar().show();
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView.addItemDecoration(
                new HorizontalDividerItemDecoration.Builder(activity)
                        .build());
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new VkWallPaginationAdapter(activity, new VkWallPaginationAdapter.Listener() {

            @Override
            public void onClickImage(VkDocsModel vkDocsModel) {
                router.navigateTo(Screens.FRAGMENT_FULLSCREEN, new FragmentImageView.InstantiateObject(vkDocsModel.getUrl(), vkDocsModel.getTitle(),vkDocsModel.getType()));
            }

            @Override
            public void onClickButtonDelete(VkDocsModel VkDocsModel) {
                presenter.delete(VkDocsModel.getId());
                router.showSystemMessage("delete");
                adapter.deleteModel(VkDocsModel);
            }

            @Override
            public void onClickButtonDownload(VkDocsModel VkDocsModel) {
                presenter.download(VkDocsModel.getUrl(), VkDocsModel.getTitle(), authPreferences.getFilePath());
            }

            @Override
            public void onClickSent(VkDocsModel vkDocsModel) {
//                router.navigateTo(Screens.FRAGMENT_SEND_MESSAGE, new FragmentSendMessage.InstantiateObject(vkDocsModel.getId()));
            }
        }).setRequestLoadNextListener(new RequestLoadNextController.OnRequestLoadNextListener() {
            @Override
            public void onRequestLoad() {
                presenter.load(adapter.getItemCount());
            }
        });

        recyclerView.setAdapter(adapter);
    }


    @Override
    public int onInflateLayout() {
        return R.layout.fragment;
    }

    @Override
    public boolean onBackPressed() {
        if (presenter != null) return presenter.onBackPressed();
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public void onDataLoaded(List<VkDocsModel> list) {
        if (adapter != null)
            if (adapter.getItemCount() == 0) adapter.setModels(list, true);
            else adapter.addModels(list);
    }

    @Override
    public void onDataError(String message) {
        if (message != null)
            router.showSystemMessage(message);
    }

    @Override
    public void onDataLoaded(VkDocsSaveResponse vkDocsSaveResponse) {
        if (adapter != null) adapter.addModel(vkDocsSaveResponse.getVkDocsModel());
        if (recyclerView != null) recyclerView.scrollToPosition(0);
    }

    @Override
    public void onDataLoading(Integer percent) {

    }

    @Override
    public void onLoadingFinished(String finished) {
        if (finished != null)
            router.showSystemMessage(finished);
    }

    @Override
    public void onLoadingError(String error) {
        if (error != null)
            router.showSystemMessage(error);
    }

    @Override
    public void unknownException() {
        if (router != null) router.exitWithMessage(getString(R.string.error_unknown));
    }

    @Override
    public void apiException(String message) {
        if (message != null && message.length() > 0) {
            if (router != null) router.exitWithMessage(message);
        } else unknownException();
    }

    @Override
    public void connectionException() {
        if (router != null) router.exitWithMessage(getString(R.string.error_connection));
    }

}
