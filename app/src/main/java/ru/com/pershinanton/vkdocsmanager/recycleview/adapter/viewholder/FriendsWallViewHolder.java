package ru.com.pershinanton.vkdocsmanager.recycleview.adapter.viewholder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.yqritc.recyclerviewflexibledivider.VerticalDividerItemDecoration;

import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.VkFriendsWallAdapter;


public class FriendsWallViewHolder extends RecyclerView.ViewHolder {
    public final TextView firstName;
    public final TextView secondName;
    public final RoundedImageView imageView;
    public final RecyclerView friendsRecyclerView;
    public VkFriendsWallAdapter vkFriendsWallAdapter;

    public FriendsWallViewHolder(View view) {
        super(view);
        firstName = view.findViewById(R.id.firstName);
        secondName = view.findViewById(R.id.secondName);
        imageView = view.findViewById(R.id.imageView);

        friendsRecyclerView = view.findViewById(R.id.friendsRecyclerView);
        friendsRecyclerView.addItemDecoration(
                new VerticalDividerItemDecoration.Builder(view.getContext())
                        .color(Color.RED)
                        .build());
    }

}