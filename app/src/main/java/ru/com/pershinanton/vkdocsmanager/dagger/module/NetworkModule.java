package ru.com.pershinanton.vkdocsmanager.dagger.module;

import android.app.Application;
import android.util.Log;

import okhttp3.Interceptor;
import okhttp3.Response;
import ru.com.pershinanton.vkdocsmanager.BuildConfig;
import ru.com.pershinanton.vkdocsmanager.BusAction;
import ru.com.pershinanton.vkdocsmanager.BusDownload;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.retrofit.ApiClient;
import com.google.gson.Gson;
import com.hwangjr.rxbus.RxBus;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.com.pershinanton.vkdocsmanager.retrofit.DownloadFileApiClient;
import ru.com.pershinanton.vkdocsmanager.retrofit.OnAttachmentDownloadListener;
import ru.com.pershinanton.vkdocsmanager.retrofit.ProgressResponseBody;
import ru.com.pershinanton.vkdocsmanager.retrofit.UploadFileApiClient;

import static com.google.android.exoplayer2.ExoPlayerLibraryInfo.TAG;


@Module
@Singleton
public class NetworkModule {

    public static final String CACHE_DIR = "requestsaver";
    public static final int MAX_LOGS_COUNT = 100;
    private static final String ASSETS_MOCK_DIR = "mock";
    private static final boolean USE_SERVER_IF_MOCK_NOT_EXISTS = true;

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Application application) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClientBuilder
                    .addNetworkInterceptor(logging);
        }

//        ConnectionSpec spec = new
//                ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
//                .tlsVersions(TlsVersion.TLS_1_2)
//                .cipherSuites(
//                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
//                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
//                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
//                .build();

        okHttpClientBuilder.hostnameVerifier((hostname, session) -> true)
//                .connectionSpecs(Collections.singletonList(spec))
        ;
        X509TrustManager trustManager = new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                X509Certificate[] cArrr = new X509Certificate[0];
                return cArrr;
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }
        };
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{trustManager}, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            okHttpClientBuilder.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        return okHttpClientBuilder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient client, Gson gson) {
//        jsonapiConverterFactory.enableSerializationOption(SerializationFeature.INCLUDE_RELATIONSHIP_ATTRIBUTES);
        return new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(ApiClient.BASE_URL)
                .build();
    }

//    @Provides
//    @Singleton
//    Converter<ResponseBody, ErrorResponse> errorResponseConverter(Retrofit provideRetrofit) {
//        return provideRetrofit.responseBodyConverter(ErrorResponse.class, new Annotation[0]);
//    }

    @Provides
    @Singleton
    ApiClient provideApiClient(Retrofit retrofit) {
        return retrofit.create(ApiClient.class);
    }

    @Provides
    @Singleton
    UploadFileApiClient provideUploadFileApiClient(OkHttpClient client, Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://vk.com")
                .build();
        return retrofit.create(UploadFileApiClient.class);
    }
    @Provides
    @Singleton
    DownloadFileApiClient provideDownloadFileApiClient(Gson gson){
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS);

            okHttpClientBuilder.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {


                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), new OnAttachmentDownloadListener() {
                                @Override
                                public void onAttachmentDownloadedSuccess() {
                                }

                                @Override
                                public void onAttachmentDownloadedError() {
                                    RxBus.get().post(BusDownload.DOWNLOAD_ERROR, true);
                                }

                                @Override
                                public void onAttachmentDownloadedFinished() {
                                }

                                @Override
                                public void onAttachmentDownloadUpdate(int percent) {
                                    RxBus.get().post(BusDownload.DOWNLOAD_PERCENT, percent);
//
                                }
                            }))
                            .build();
                }
            });



        okHttpClientBuilder.hostnameVerifier((hostname, session) -> true)
        ;
        X509TrustManager trustManager = new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                X509Certificate[] cArrr = new X509Certificate[0];
                return cArrr;
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
            }
        };
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{trustManager}, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            okHttpClientBuilder.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        OkHttpClient client= okHttpClientBuilder.build() ;

        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://vk.com")
                .build();
        return retrofit.create(DownloadFileApiClient.class);

    }

}
