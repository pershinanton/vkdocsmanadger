package ru.com.pershinanton.vkdocsmanager.recycleview.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.model.SizesModel;
import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.utils.RequestLoadNextController;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.viewholder.WallDocsViewHolder;
import ru.com.pershinanton.vkdocsmanager.retrofit.ApiClient;
import ru.com.pershinanton.vkdocsmanager.utils.StartSnapHelper;

import static android.support.v7.widget.LinearLayoutManager.HORIZONTAL;


public class VkWallPaginationAdapter extends RecyclerView.Adapter<WallDocsViewHolder> {

    @Inject
    Toaster toaster;

    private final List<VkDocsModel> wallModels = new ArrayList<>();

    private final RequestLoadNextController needLoadNextController = new RequestLoadNextController();
    private Listener listener;
    private LayoutInflater inflater;
    private Context context;
    private String title;


    public VkWallPaginationAdapter(Context context, Listener listener) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public WallDocsViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.vk_wall_item, parent, false);
        return new WallDocsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WallDocsViewHolder viewHolder, final int position) {
        needLoadNextController.checkIsNeedLoadNext(getItemCount(), position);
        new StartSnapHelper().attachToRecyclerView(viewHolder.docsRecyclerView);
        title = wallModels.get(position).getTitle();
        viewHolder.title.setText(title);
        viewHolder.ext.setText(wallModels.get(position).getExt());
        String dateString = DateFormat.format("dd-MM-yyyy в kk:mm", new Date(wallModels.get(position).getDate() )).toString();
        viewHolder.date.setText(dateString);
        viewHolder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context, v);
                popupMenu.inflate(R.menu.popup_menu);
                popupMenu
                        .setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.menu1:
                                        if (listener != null)
                                            listener.onClickButtonDelete(wallModels.get(position));
                                        return true;
                                    case R.id.menu2:
                                        if (listener != null)
                                            listener.onClickButtonDownload(wallModels.get(position));
                                        return true;

                                    default:
                                        return false;
                                }
                            }
                        });
                popupMenu.show();
            }
        });
        viewHolder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onClickImage(wallModels.get(position));
            }
        });

        initRecyclerView(viewHolder, position);
    }

    private void initRecyclerView(WallDocsViewHolder viewHolder, int position) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, HORIZONTAL, false);
        viewHolder.docsRecyclerView.setLayoutManager(layoutManager);
        viewHolder.imageView.setImageDrawable(null);
        VkDocsModel vkDocsModel = wallModels.get(position);
        if (vkDocsModel.getPreviewModel() != null) {
            SizesModel model = null;
            for (int i = 0; i < vkDocsModel.getPreviewModel().getPhotoModel().getSizes().size(); i++) {
                SizesModel sizesModel = vkDocsModel.getPreviewModel().getPhotoModel().getSizes().get(i);
                if (sizesModel.getHeight() >= 600 || sizesModel.getWidth() >= 600) {
                    model = sizesModel;
                    break;
                } else if (i == vkDocsModel.getPreviewModel().getPhotoModel().getSizes().size() - 1) {
                    model = sizesModel;
                }
            }
            if (model != null)
                Glide.with(context)
                        .load(model.getSrc())
                        .into(viewHolder.imageView);

        } else {
            switch (vkDocsModel.getType()) {
                case VkDocsModel.TYPE_PDF:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_pdf);
                    break;
                case VkDocsModel.TYPE_ARHIVE:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_wall_docs);
                    break;
                case VkDocsModel.TYPE_VIDEO:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_video);
                    break;
                case VkDocsModel.TYPE_BOOKS:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_books);
                    break;
                case VkDocsModel.TYPE_AUDIO:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_audio);
                    break;
                case VkDocsModel.TYPE_IMAGE:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_image);
                    break;
                case VkDocsModel.TYPE_UNKNOW:
                    viewHolder.imageView.setImageResource(R.drawable.zzz_unknow);
                    break;

            }
            viewHolder.docsRecyclerView.setAdapter(viewHolder.vkWallDocsAdapter);
        }
    }

    @Override
    public int getItemCount() {
        return wallModels.size();
    }


    public void setModels(List<VkDocsModel> models, boolean needLoadNextControllerEnabled) {
        this.wallModels.clear();
        if (models != null)
            this.wallModels.addAll(models);
        needLoadNextController.resetNeedLoadNextCounter();
        needLoadNextController.setEnabled(needLoadNextControllerEnabled);
        notifyDataSetChanged();
    }

    public void addModels(List<VkDocsModel> models) {
        int oldSize = this.wallModels.size();
        if (models != null)
            this.wallModels.addAll(models);
        notifyItemRangeInserted(oldSize, models.size());
    }

    public void addModel(VkDocsModel model) {
        if (model != null)
            this.wallModels.add(0, model);
        notifyItemInserted(0);
    }

    public void deleteModel(VkDocsModel model) {
        if (model != null)
            for (int i = 0; i < wallModels.size(); i++) {
                if (wallModels.get(i).getId() == (model.getId())) {
                    this.wallModels.remove(i);
                    notifyItemRemoved(i);
                    break;
                }
            }
    }

    public void resetNeedLoadNextCounter() {
        needLoadNextController.resetNeedLoadNextCounter();
    }

    public VkWallPaginationAdapter setRequestLoadNextListener(RequestLoadNextController.OnRequestLoadNextListener onRequestLoadNextListener) {
        needLoadNextController.setRequestLoadNextListener(ApiClient.ITEMS_PER_PAGE / 2, onRequestLoadNextListener);
        return this;
    }

    public interface Listener {
        void onClickImage(VkDocsModel VkDocsModel);

        void onClickButtonDelete(VkDocsModel VkDocsModel);

        void onClickButtonDownload(VkDocsModel VkDocsModel);

        void onClickSent(VkDocsModel vkDocsModel);
    }


}
