package ru.com.pershinanton.vkdocsmanager.main.base;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface BaseNetworkMvpView extends MvpView {
    void unknownException();

    void apiException(String message);

    void connectionException();
}
