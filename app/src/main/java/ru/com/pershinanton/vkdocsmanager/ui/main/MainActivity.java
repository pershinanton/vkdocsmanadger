package ru.com.pershinanton.vkdocsmanager.ui.main;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.BusAction;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.Screens;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.ui.base.BaseActivity;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.com.pershinanton.vkdocsmanager.utils.PackageHepler;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.BackTo;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;


public class MainActivity extends BaseActivity {

    @BindView(R.id.bottomNavigationView)
    BottomNavigationView bottomNavigationView;
    @Inject
    Toaster toaster;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    NavigatorHolder navigatorHolder;

    @Subscribe(
            thread = EventThread.MAIN_THREAD,
            tags = {
                    @Tag(BusAction.AUTH_STATE_UNAUTHORIZED)
            }
    )
    public void authStateUnauthorized(Boolean authorized) {
        backToMainFragment();
    }

    public void backToMainFragment() {
        if (navigator != null)
            navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_LOGIN, null)});

    }

    private final Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(), R.id.container) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.FRAGMENT_DOCSWALL:
                    return FragmentWall.getNewInstance(/*(FragmentWall.InstantiateObject) data*/);
                case Screens.FRAGMENT_LOGIN:
                    return FragmentLogin.getNewInstance();
                case Screens.FRAGMENT_FULLSCREEN:
                    return FragmentImageView.getNewInstance((FragmentImageView.InstantiateObject) data);
                case Screens.FRAGMENT_PROFILE:
                    return FragmentProfile.getNewInstance();
//                case Screens.FRAGMENT_SEND_MESSAGE:
//                    return FragmentSendMessage.getNewInstance((FragmentSendMessage.InstantiateObject) data);
                case Screens.FRAGMENT_SETTINGS:
                    return FragmentSettings.getNewInstance();
//                case Screens.FRAGMENT_SEARCH:
//                    return FragmentSearchMessage.getNewInstance((FragmentSearchMessage.InstantiateObject) data);
            }
            return null;
        }

        @Override
        protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
            fragmentTransaction.commitAllowingStateLoss();
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }

        @Override
        public void applyCommands(Command[] commands) {
            try {
                super.applyCommands(commands);
                getSupportFragmentManager().executePendingTransactions();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        System.out.println("KEY HASH: " + PackageHepler.getKeyHash(this));
        System.out.println("KEY SHA1: " + PackageHepler.getCertificateSHA1Fingerprint(this, true));
        super.onCreate(savedInstanceState);
    }

    @Override
    public int onInflateLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewInflated(Bundle savedInstanceState) {
        initButterKnife();
        initRxBus();

        if (authPreferences.isSigned()) {
            navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_DOCSWALL, null/* new FragmentWall.InstantiateObject(MovieList.getList())*/)});
        } else {
            navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_LOGIN, null/* new FragmentWall.InstantiateObject(MovieList.getList())*/)});
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_profile:
                    navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_PROFILE, null)});
                    break;
                case R.id.action_wall_docs:
                    navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_DOCSWALL, null)});
                    break;
                case R.id.action_settings:
                    navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_SETTINGS, null)});
                    break;
            }
            return true;
        });
    }

    @Override
    protected void onResumeFragments() {
        if (navigatorHolder != null) {
            super.onResumeFragments();
            navigatorHolder.setNavigator(navigator);
        }
    }

    @Override
    protected void onPause() {
        if (navigatorHolder != null) navigatorHolder.removeNavigator();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (bottomNavigationView != null) bottomNavigationView.animate().setListener(null).cancel();
        super.onDestroy();
    }

    public void showBottomNavigationView() {
        if (bottomNavigationView != null) {
            bottomNavigationView.setVisibility(View.VISIBLE);
            bottomNavigationView.animate().setListener(null).cancel();
            bottomNavigationView.animate().translationY(0).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (bottomNavigationView != null)
                        bottomNavigationView.animate().setListener(null);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    if (bottomNavigationView != null)
                        bottomNavigationView.animate().setListener(null);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
        }
    }

    public void hideBottomNavigationView() {
        if (bottomNavigationView != null) {
            bottomNavigationView.animate().setListener(null).cancel();
            bottomNavigationView.animate().translationY(bottomNavigationView.getHeight()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    bottomNavigationView.setVisibility(View.GONE);
                    if (bottomNavigationView != null)
                        bottomNavigationView.animate().setListener(null);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                    if (bottomNavigationView != null)
                        bottomNavigationView.animate().setListener(null);
                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                authPreferences.setAccessToken(res.accessToken);
                authPreferences.setUserId(res.userId);
                navigator.applyCommands(new Command[]{new BackTo(null), new Replace(Screens.FRAGMENT_DOCSWALL, null/* new FragmentWall.InstantiateObject(MovieList.getList())*/)});
            }

            @Override
            public void onError(VKError error) {
                toaster.toastLong(error.errorReason);
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragment != null
                && fragment instanceof BackButtonListener
                && ((BackButtonListener) fragment).onBackPressed()) {
            return;
        } else {
            super.onBackPressed();
        }
    }


}