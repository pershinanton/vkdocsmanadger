package ru.com.pershinanton.vkdocsmanager.ui.main;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.developer.filepicker.controller.DialogSelectionListener;
import com.developer.filepicker.model.DialogConfigs;
import com.developer.filepicker.model.DialogProperties;
import com.developer.filepicker.view.FilePickerDialog;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.ui.base.BaseFragment;
import ru.terrakok.cicerone.Router;

public class FragmentSettings extends BaseFragment {
    @BindView(R.id.enterPath)
    Button enter;
    @BindView(R.id.background)
    ImageView background;
    @Inject
    Router router;
    MainActivity activity;
    @Inject
    Toaster toaster;
    @Inject
    AuthPreferences authPreferences;

    public FragmentSettings() {
        super(true, false, Orientation.NONE);
    }

    public static FragmentSettings getNewInstance() {
        FragmentSettings fragment = new FragmentSettings();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    public static String uriFromDrawableResource(Integer resId) {
        return "drawable://" + resId;
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        activity.getSupportActionBar().setTitle(R.string.settings);
        activity.showBottomNavigationView();
        Glide.with(this)
                .asBitmap()
                .load(R.drawable.settings)
                .apply(new RequestOptions().override(1600, 1600))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        background.setImageBitmap(resource);
                    }
                });

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogProperties properties = new DialogProperties();
                properties.selection_mode = DialogConfigs.SINGLE_MODE;
                properties.selection_type = DialogConfigs.DIR_SELECT;
                properties.root = new File(DialogConfigs.DEFAULT_DIR);
                properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
                properties.offset = new File(DialogConfigs.DEFAULT_DIR);
                properties.extensions = null;
                FilePickerDialog dialog = new FilePickerDialog(activity, properties);
                dialog.setTitle("Выберите папку");
                dialog.setDialogSelectionListener(new DialogSelectionListener() {
                    @Override
                    public void onSelectedFilePaths(String[] files) {
                        if (files != null && files.length > 0) {
                            String filePath = files[0];
                            authPreferences.setFilePath(filePath);
                        }
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int onInflateLayout() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

}
