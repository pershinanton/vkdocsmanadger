//package ru.com.pershinanton.vkdocsmanager.main;
//
//import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
//import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
//import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
//
//import java.util.List;
//
//import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpView;
//import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;
//
//public interface FragmentSearchMvpView extends BaseNetworkMvpView {
//    @StateStrategyType(AddToEndStrategy.class)
//    void onDataLoaded(List<VkFriendsModel> list);
//
//    @StateStrategyType(OneExecutionStateStrategy.class)
//    void onDataError(String message);
//
//
//
//
//}
