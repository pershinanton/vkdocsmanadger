package ru.com.pershinanton.vkdocsmanager;

import com.hwangjr.rxbus.RxBus;
import com.vk.sdk.VKSdk;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;

@Singleton
public class LogoutController {

    AuthPreferences authPreferences;

    @Inject
    public LogoutController(AuthPreferences authPreferences) {
        this.authPreferences = authPreferences;
    }

    public void unauthorized() {
        VKSdk.logout();
        authPreferences.clear();
        RxBus.get().post(BusAction.AUTH_STATE_UNAUTHORIZED, true);
    }

}