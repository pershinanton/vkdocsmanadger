package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;


public class VkDocsUploadFileResponse {
    @SerializedName("file")
    private String file;

    @SerializedName("error_descr")
    private String error;

    public String getFile() {
        return file;
    }
    public String getError() {
        return error;
    }
}
