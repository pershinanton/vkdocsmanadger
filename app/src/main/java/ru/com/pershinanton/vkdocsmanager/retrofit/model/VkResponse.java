package ru.com.pershinanton.vkdocsmanager.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VkResponse<T> {// <T> указывается какой будет тип объекта response
    @SerializedName("error")
    @Expose
    public VkErrorResponse error;
    @SerializedName("response")
    @Expose
    public T response = null;

    public VkErrorResponse getError() {
        return error;
    }

    public T getResponse() {
        return response;
    }
}