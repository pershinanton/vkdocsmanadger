package ru.com.pershinanton.vkdocsmanager.utils;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Handler;
import android.view.Surface;
import android.view.TextureView;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

public class VideoPlayerController implements Player.EventListener {


    private final String uuid;
    private final boolean looping;
    private final Handler mainHandler = new Handler();
    private final BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
    private final TrackSelection.Factory videoTrackSelectionFactory =
            new AdaptiveTrackSelection.Factory(bandwidthMeter);
    private final TrackSelector trackSelector =
            new DefaultTrackSelector(videoTrackSelectionFactory);
    private final RenderersFactory renderersFactory;
    private final LoadControl loadControl = new DefaultLoadControl(new DefaultAllocator(true, C.DEFAULT_BUFFER_SEGMENT_SIZE),
            360000, 600000, 1000, 5000,
            C.LENGTH_UNSET,
            false);
    private final Handler progressUpdateHandler = new Handler();
    private VideoPlayerControllerListener listener;
    private TextureView textureView;
    private Context context;
    private SimpleExoPlayer player;
    private final Runnable progressUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            if (player != null) {
                listener.onProgress(textureView, player, player.getCurrentPosition());
                progressUpdateHandler.postDelayed(progressUpdateRunnable, 100);
            }
        }
    };
    //    private Runnable callback;
    private State state;
    //    private CacheListener cacheListener;
    private boolean muted;
    private int currentState;

    public VideoPlayerController(TextureView textureView, String uuid, boolean looping, VideoPlayerController.VideoPlayerControllerListener listener) {
        this.textureView = textureView;
        this.context = textureView.getContext();
        this.uuid = uuid;
        this.looping = looping;
        this.listener = listener;

        renderersFactory = new DefaultRenderersFactory(context);
    }

    public static void adjustAspectRatio(TextureView textureView, int videoWidth, int videoHeight) {
        int viewWidth = textureView.getWidth();
        int viewHeight = textureView.getHeight();
        double videoRatio = (double) videoHeight / videoWidth;

        int newWidth, newHeight;

        if (viewHeight < (int) (viewWidth * videoRatio)) {
            newWidth = viewWidth;
            newHeight = (int) (viewWidth * videoRatio);
        } else {
            newWidth = (int) (viewHeight / videoRatio);
            newHeight = viewHeight;
        }

//        System.out.println("!!!!!!!! " + " videoWidth " + videoWidth + " videoHeight " + videoHeight);
//        System.out.println("!!!!!!!! " + " viewWidth " + viewWidth + " viewHeight " + viewHeight);
//        System.out.println("!!!!!!!! " + " newWidth " + newWidth + " newHeight " + newHeight);

        int xoff = (viewWidth - newWidth) / 2;
        int yoff = (viewHeight - newHeight) / 2;

        Matrix txform = new Matrix();
        textureView.getTransform(txform);
        txform.setScale((float) newWidth / viewWidth, (float) newHeight / viewHeight);
        txform.postTranslate(xoff, yoff);
        textureView.setTransform(txform);
    }

    public boolean isPlaying() {
        try {
            return state != null && state.equals(State.PLAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isMuted() {
        return muted;
    }

    public void mute() {
        muted = true;
        if (player != null) player.setVolume(0);
    }

    public void unmute() {
        muted = false;
        if (player != null) player.setVolume(1);
    }


    public void fitToWidth() {
        if (player != null && player.getVideoFormat() != null)
            adjustAspectRatio(textureView, player.getVideoFormat().width, player.getVideoFormat().height);
    }

    public void play(final String videoUrl, long startPosition) {
        if (player != null) return;
        state = State.PLAY;
        if (listener != null)
            listener.onStateChanged(textureView, player, state, isPlaying());
        player = ExoPlayerFactory.newSimpleInstance(renderersFactory, trackSelector, loadControl);
        if (isMuted()) mute();
        else unmute();

//        if(cacheListener!=null)App.getProxy(context).unregisterCacheListener(cacheListener);
//        cacheListener = new CacheListener() {
//            @Override
//            public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
//                System.out.println(url);
//            }
//        };
//        App.getProxy(context).registerCacheListener(cacheListener, videoUrl);

//        if (textureView.isAvailable()) {
//            textureViewAvalable(videoUrl);
//        } else {
//            textureView.setSurfaceTextureListener(new SweetSurfaceTextureListener() {
//                @Override
//                public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
//                    textureView.setSurfaceTextureListener(null);
//                    textureViewAvalable(videoUrl);
//                }
//            });
//        }

//            final HttpProxyCacheServer proxy = App.getProxy();

        final Surface surface = new Surface(textureView.getSurfaceTexture());
        try {
//                AssetFileDescriptor afd = context.getAssets().openFd("video.webm");
//                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());

//                player.setDataSource(Uri.parse(proxy.getProxyUrl(videoUrl)));

            player.removeListener(this);
            player.addListener(this);
            player.setVideoSurface(surface);

            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                    Util.getUserAgent(context, Util.getUserAgent(context, uuid)));

            if (videoUrl.contains(".m3u8")) {
                player.prepare(new HlsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(videoUrl), mainHandler, null));
            } else {
                player.prepare(new ExtractorMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(videoUrl)));
            }
            if (startPosition > 0)
                player.seekTo(startPosition);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            stop();
        } catch (SecurityException e) {
            e.printStackTrace();
            stop();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            stop();
        }

    }

//    private void textureViewAvalable(String videoUrl) {
//        if (player != null) {
////            final HttpProxyCacheServer proxy = App.getProxy();
//            final Surface surface = new Surface(textureView.getSurfaceTexture());
//            try {
////                AssetFileDescriptor afd = context.getAssets().openFd("video.webm");
////                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
//
////                player.setDataSource(Uri.parse(proxy.getProxyUrl(videoUrl)));
//
//                player.removeListener(this);
//                player.addListener(this);
//                player.setVideoSurface(surface);
//
//                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
//                        Util.getUserAgent(context, Util.getUserAgent(context, uuid)));
//
//                if (videoUrl.containsDownloadInProgressModel(".m3u8")) {
//                    player.prepare(new HlsMediaSource.Factory(dataSourceFactory)
//                            .createMediaSource(Uri.parse(videoUrl), mainHandler, null));
//                } else {
//                    player.prepare(new ExtractorMediaSource.Factory(dataSourceFactory)
//                            .createMediaSource(Uri.parse(videoUrl)));
//                }
//                player.seekTo();
//            } catch (IllegalArgumentException e) {
//                e.printStackTrace();
//                stop();
//            } catch (SecurityException e) {
//                e.printStackTrace();
//                stop();
//            } catch (IllegalStateException e) {
//                e.printStackTrace();
//                stop();
//            }
//        }
//    }

    public void swapUrl(String videoUrl) {
        if (player != null) {
            try {
                DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context,
                        Util.getUserAgent(context, Util.getUserAgent(context, uuid)));

                if (videoUrl.contains(".m3u8")) {
                    player.prepare(new HlsMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(videoUrl), mainHandler, null), false, false);
                } else {
                    player.prepare(new ExtractorMediaSource.Factory(dataSourceFactory)
                            .createMediaSource(Uri.parse(videoUrl)), false, false);
                }
                play();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                stop();
            } catch (SecurityException e) {
                e.printStackTrace();
                stop();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                stop();
            }
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (currentState != playbackState)
            switch (playbackState) {
                case Player.STATE_ENDED:
                    if (looping)
                        player.seekTo(0);
                    else {
//                        pause();
                        if (listener != null)
                            listener.onComplete(textureView, player);
                    }
                    break;
                case Player.STATE_READY:
                    if (isPlaying())
                        play();
                    if (listener != null)
                        listener.onReady(textureView, player);
                    break;
                case Player.STATE_BUFFERING:
                    if (listener != null)
                        listener.onBuffering(textureView, player);
                    player.setPlayWhenReady(true);
                    startTimer();
                    break;
            }
        currentState = playbackState;
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        if (listener != null)
            listener.onError(textureView, player);
        stop();
    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

//    private class onBufferingUpdate implements Runnable {
//        @Override
//        public void run() {
//            final int percent = player.getBufferedPercentage();
//            JZMediaManager.instance().mainThreadHandler.post(new Runnable() {
//                @Override
//                public void run() {
//                    if (JZVideoPlayerManager.getCurrentJzvd() != null) {
//                        JZVideoPlayerManager.getCurrentJzvd().setBufferProgress(percent);
//                    }
//                }
//            });
//            if(percent < 100) {
//                mainHandler.postDelayed(callback, 300);
//            } else {
//                mainHandler.removeCallbacks(callback);
//            }
//        }
//    }

    private void startTimer() {
        progressUpdateHandler.removeCallbacks(progressUpdateRunnable);
        progressUpdateHandler.postDelayed(progressUpdateRunnable, 100);
//        Timer timer = new Timer();
//        timer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                if (player != null)
//                    player.getCurrentPosition();
//            }
//        },0,1000);
    }

    public void setTextureView(TextureView textureView) {
        if (textureView != null) {
            if (textureView.isAvailable()) {
                player.setVideoSurface(new Surface(textureView.getSurfaceTexture()));
            } else {
                textureView.setSurfaceTextureListener(new SweetSurfaceTextureListener() {
                    @Override
                    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
                        textureView.setSurfaceTextureListener(null);
                        player.setVideoSurface(new Surface(textureView.getSurfaceTexture()));
                    }
                });
            }
        } else {
            player.setVideoSurface(null);
        }
    }

    public void stop() {
        progressUpdateHandler.removeCallbacks(progressUpdateRunnable);
        if (player != null) {
            state = State.STOP;
            if (listener != null)
                listener.onStateChanged(textureView, player, state, isPlaying());
            player.removeListener(this);
            player.stop();
            player.release();
            player = null;
        }
        if (textureView != null)
            textureView.setSurfaceTextureListener(null);
    }

    public void pause() {
        progressUpdateHandler.removeCallbacks(progressUpdateRunnable);
        if (player != null) {
            state = State.PAUSE;
            if (listener != null)
                listener.onStateChanged(textureView, player, state, isPlaying());
            player.setPlayWhenReady(false);
        }
    }

    public void play() {
        progressUpdateHandler.removeCallbacks(progressUpdateRunnable);
        progressUpdateHandler.postDelayed(progressUpdateRunnable, 100);
        if (player != null) {
            state = State.PLAY;
            if (listener != null)
                listener.onStateChanged(textureView, player, state, isPlaying());
            player.setPlayWhenReady(true);
        }
    }

    public void release() {
        stop();
        listener = null;
//        if(cacheListener!=null){
//            App.getProxy(context).unregisterCacheListener(cacheListener);
//            cacheListener = null;
//        }
        if (textureView != null) {
            textureView.setSurfaceTextureListener(null);
            textureView = null;
        }
        context = null;
    }

    public void seekTo(long positionMs) {
        if (player != null) {
//            if(player.getPlaybackState() == )
            player.seekTo(positionMs);
            play();
//            if(isPlaying())
//
        }
    }

    public boolean isComplete() {
        if (player != null)
            return player.getDuration() == player.getCurrentPosition();
        return false;
    }

    public long getCurrentPostion() {
        if (player != null)
            return player.getCurrentPosition();
        return 0;
    }

    public void setVolume(float volume) {
        if (player != null) player.setVolume(volume);
    }

    public enum State {
        PLAY, PAUSE, STOP
    }

    public interface VideoPlayerControllerListener {
        void onError(TextureView textureView, SimpleExoPlayer player);

        void onReady(TextureView textureView, SimpleExoPlayer player);

        void onStateChanged(TextureView textureView, SimpleExoPlayer player, State state, boolean isPlaying);

        void onProgress(TextureView textureView, SimpleExoPlayer player, long currentPosition);

        void onComplete(TextureView textureView, SimpleExoPlayer player);

        void onBuffering(TextureView textureView, SimpleExoPlayer player);
    }

}