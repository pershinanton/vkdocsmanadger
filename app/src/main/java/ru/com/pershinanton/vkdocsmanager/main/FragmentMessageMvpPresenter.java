//package ru.com.pershinanton.vkdocsmanager.main;
//
//import com.arellomobile.mvp.InjectViewState;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import io.reactivex.Flowable;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.Disposable;
//import io.reactivex.schedulers.Schedulers;
//import retrofit2.Response;
//import ru.com.pershinanton.vkdocsmanager.App;
//import ru.com.pershinanton.vkdocsmanager.LogoutController;
//import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
//import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpPresenter;
//import ru.com.pershinanton.vkdocsmanager.main.base.UnauthorizedIOException;
//import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;
//import ru.com.pershinanton.vkdocsmanager.model.VkMessageModel;
//import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
//import ru.com.pershinanton.vkdocsmanager.retrofit.ApiClient;
//import ru.com.pershinanton.vkdocsmanager.retrofit.exception.ApiException;
//import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkFriendsGetResponse;
//import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkResponse;
//import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkSendMessageResponse;
//import ru.com.pershinanton.vkdocsmanager.rxjava.RetryWithDelay;
//import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
//import ru.terrakok.cicerone.Router;
//
//
//@InjectViewState
//public class FragmentMessageMvpPresenter extends BaseNetworkMvpPresenter<FragmentMessageMvpView> implements BackButtonListener, RetryWithDelay.Listener {// в этом классе мы получаем данные из других мест
//    @Inject
//    Toaster toaster;
//    @Inject
//    AuthPreferences authPreferences;
//    @Inject
//    ApiClient apiClient;
//    @Inject
//    LogoutController logoutController;
//    private Router router;
//    private Disposable disposable;
//
//    public FragmentMessageMvpPresenter(Router router) {
//        super(false);
//        this.router = router;
//        App.INSTANCE.getAppComponent().inject(this);
//    }
//
//    @Override
//    protected void onFirstViewAttach() {
//        super.onFirstViewAttach();
//        load(0);
//    }
//
//    public void load(int offset) {
//        disposable = Flowable.just(true)
////                .delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.newThread())
////                    .retry(throwable -> onRequestRetryNetwork(throwable))
////                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
//                .map((Boolean value) -> {
//                    Response<VkResponse<VkFriendsGetResponse>> response;
//                    try {                                           // response ответ с сервера по обращению
//                        response = apiClient.friendsGet(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken(), authPreferences.getUserId(), "hints", 20, offset, "photo_200_orig", "dat").execute();
//                    } catch (Throwable throwable) {
//                        throw new Exception(throwable);
//                    }
//                    if (response == null) throw new ApiException();
//                    if (response.code() != 200) throw new ApiException();
//                    if (response.errorBody() != null) throw new ApiException();
//                    if (response.body() == null) throw new ApiException();
//                    VkResponse<VkFriendsGetResponse> apiResponse = response.body();
//
//                    if (apiResponse.getError() != null && apiResponse.getError().getErrorCode() > 0) {
//                        if (apiResponse.getError().getErrorCode() == 5) {
//                            logoutController.unauthorized();
//                            throw new UnauthorizedIOException();
//                        } else {
//                            if (apiResponse != null && apiResponse.getError().getErrorMsg() != null && apiResponse.getError().getErrorMsg().length() > 0)
//                                throw new ApiException(apiResponse.getError().getErrorMsg());
//                            else
//                                throw new ApiException();
//                        }
//                    }
//
//                    VkFriendsGetResponse result = apiResponse.response;
//                    if (result == null)
//                        throw new ApiException();
//                    List<VkFriendsModel> items = result.getItems();
//                    if (items == null) throw new ApiException();
//
//                    return items;
//
//                }).observeOn(AndroidSchedulers.mainThread())
//                .subscribe(list -> {
//                    getViewState().onDataLoaded(list);
//                }, throwable -> {
//                    onThrowable(throwable);
//                });
//    }
//
//    public void send(int userId, String attachment) {
//        disposable = Flowable.just(true)
////                .delay(2, TimeUnit.SECONDS)
//                .subscribeOn(Schedulers.newThread())
////                    .retry(throwable -> onRequestRetryNetwork(throwable))
////                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
//                .map((Boolean value) -> {
//                    Response<VkSendMessageResponse> response;
//                    try {                                           // response ответ с сервера по обращению
//                        response = apiClient.messageSend(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken(), userId, attachment).execute();
//                    } catch (Throwable throwable) {
//                        throw new Exception(throwable);
//                    }
//                    if (response == null) throw new ApiException();
//                    if (response.code() != 200) throw new ApiException();
//                    if (response.errorBody() != null) throw new ApiException();
//                    if (response.body() == null) throw new ApiException();
//                    VkSendMessageResponse apiResponse = response.body();
//
//                    if (apiResponse.getError() != null) {
//                        if (apiResponse != null && apiResponse.getError() != null && apiResponse.getError().length() > 0)
//                            throw new ApiException(apiResponse.getError());
//                        else
//                            throw new ApiException();
//                    }
//
//                    VkMessageModel result = apiResponse.response;
//                    if (result == null)
//                        throw new ApiException();
////                    if (items == null) throw new ApiException();
//
//                    return result;
//
//                }).observeOn(AndroidSchedulers.mainThread())
//                .subscribe(result -> {
////                    getViewState().onDataLoaded();/**/
//                }, throwable -> {
//                    onThrowable(throwable);
//                });
//    }
//
//    protected void dispose() {
//        if (disposable != null) {
//            if (!disposable.isDisposed()) {
//                disposable.dispose();
//            }
//            disposable = null;
//        }
//    }
//
//
//    @Override
//    public void onDestroy() {
//        dispose();
//        super.onDestroy();
//        router = null;
//    }
//
//    @Override
//    public boolean onBackPressed() {
//        if (router != null) router.exit();
//        return true;
//    }
//
//}