package ru.com.pershinanton.vkdocsmanager.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VkMessageModel {

    @SerializedName("peer_id")
    private String peerId;
    @SerializedName("message_id")
    private String messageId;
    @SerializedName("error")
    private String error;

    public String getPeerId() {
        return peerId;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getError() {
        return error;
    }
}