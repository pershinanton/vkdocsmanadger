package ru.com.pershinanton.vkdocsmanager;

import android.app.Application;

import com.fondesa.lyra.Lyra;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.reactivex.plugins.RxJavaPlugins;
import ru.com.pershinanton.vkdocsmanager.dagger.AppComponent;
import ru.com.pershinanton.vkdocsmanager.dagger.DaggerAppComponent;
import ru.com.pershinanton.vkdocsmanager.dagger.module.ApplicationModule;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.utils.UilHelper;

public class App extends Application {
    public static App INSTANCE;
    AuthPreferences authPreferences;
    private AppComponent appComponent;
    private VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                // VKAccessToken is invalid
            } else if (newToken != null) {
                authPreferences.setAccessToken(newToken.accessToken);
            }

        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        authPreferences = new AuthPreferences(this);
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
//                                .setDefaultFontPath("font/MyFont.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        Lyra.with(this).build();
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);


        ImageLoader.getInstance().init(new ImageLoaderConfiguration.Builder(this)
//                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
//                .diskCacheExtraOptions(DP.getMetrix(context).widthPixels, DP.getMetrix(context).heightPixels, null)
//                .taskExecutor()
//                .taskExecutorForCachedImages()
//                .threadPoolSize(3) // default
//                .threadPriority(Thread.NORM_PRIORITY - 2) // default
//                .threadPriority(Thread.NORM_PRIORITY - 4)
//                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .tasksProcessingOrder(QueueProcessingType.LIFO)
//                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new WeakMemoryCache())
//                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
//                .memoryCacheSize(2 * 1024 * 1024)
//                .memoryCacheSizePercentage(13) // default
//                .diskCache(new UnlimitedDiscCache(cacheDir)) // default
                .diskCacheSize(100 * 1024 * 1024)
//                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
//                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .imageDownloader(new BaseImageDownloader(this)) // default
                .imageDecoder(new BaseImageDecoder(false)) // default
//                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .defaultDisplayImageOptions(UilHelper.getOptions().build())
//                .writeDebugLogs()

                .build());

        RxJavaPlugins.setErrorHandler(throwable -> {
        });// обязательно при использовании (асинхронность действий)
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .applicationModule(new ApplicationModule(this)).build();
        }
        return appComponent;
    }

}