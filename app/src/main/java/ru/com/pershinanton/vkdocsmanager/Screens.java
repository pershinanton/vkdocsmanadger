package ru.com.pershinanton.vkdocsmanager;

public class Screens {
    public static final String FRAGMENT_DOCSWALL = "FRAGMENT_DOCSWALL";
    public static final String FRAGMENT_PROFILE = "FRAGMENT_PROFILE";
    public static final String FRAGMENT_FULLSCREEN = "FRAGMENT_FULLSCREEN";
    public static final String FRAGMENT_LOGIN = "FRAGMENT_LOGIN";
    public static final String FRAGMENT_SEND_MESSAGE = "FRAGMENT_SEND_MESSAGE";
    public static final String FRAGMENT_SETTINGS = "FRAGMENT_SETTINGS";
    public static final String FRAGMENT_SEARCH = "FRAGMENT_SEARCH";

}