package ru.com.pershinanton.vkdocsmanager.utils;

import android.graphics.Bitmap;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class UilHelper {

    public static DisplayImageOptions.Builder getOptions() {
        return new DisplayImageOptions.Builder()// resource or drawable
//                .delayBeforeLoading(1000)
//                .cacheInMemory(false) // default
//                .cacheOnDisk(false) // default
//                .preProcessor(...)
//        .postProcessor(...)
//        .extraForDownloader(...)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)

//                .bitmapConfig(Bitmap.Config.RGB_565) // default
//                .decodingOptions()
//        .displayer(new SimpleBitmapDisplayer()) // default
//                .handler(new Handler()) // default
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
//            .delayBeforeLoading(500)
                .resetViewBeforeLoading(false);
//                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
//                .imageScaleType(ImageScaleType.IN_SAMPLE_INT);

    }

}
