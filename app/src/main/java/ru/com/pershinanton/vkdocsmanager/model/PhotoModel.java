package ru.com.pershinanton.vkdocsmanager.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PhotoModel {


    @SerializedName("sizes")
    @Expose
    private List<SizesModel> sizes;

    public List<SizesModel> getSizes() {
        return sizes;
    }
}