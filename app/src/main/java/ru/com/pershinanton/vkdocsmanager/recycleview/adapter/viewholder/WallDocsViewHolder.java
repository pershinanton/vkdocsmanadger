package ru.com.pershinanton.vkdocsmanager.recycleview.adapter.viewholder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.yqritc.recyclerviewflexibledivider.VerticalDividerItemDecoration;

import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.VkWallDocsAdapter;


public class WallDocsViewHolder extends RecyclerView.ViewHolder {
    public final TextView ext;
    public final TextView title;
    public final TextView date;
    public final RoundedImageView imageView;
    public final Button removeBtn;
    public final RecyclerView docsRecyclerView;
    public VkWallDocsAdapter vkWallDocsAdapter;

    public WallDocsViewHolder(View view) {
        super(view);
        title = view.findViewById(R.id.title);
        ext = view.findViewById(R.id.ext);
        imageView = view.findViewById(R.id.imageView);
        removeBtn = view.findViewById(R.id.removeBtn);
        date = view.findViewById(R.id.date);

        docsRecyclerView = view.findViewById(R.id.docsRecyclerView);
        docsRecyclerView.addItemDecoration(
                new VerticalDividerItemDecoration.Builder(view.getContext())
                        .color(Color.RED)
                        .build());
    }

}