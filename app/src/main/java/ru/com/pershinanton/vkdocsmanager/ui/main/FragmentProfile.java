package ru.com.pershinanton.vkdocsmanager.ui.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.LogoutController;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.main.FragmentProfileMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.FragmentProfileMvpView;
import ru.com.pershinanton.vkdocsmanager.model.VkUserModel;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.ui.base.BaseFragment;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.com.pershinanton.vkdocsmanager.utils.UilHelper;
import ru.terrakok.cicerone.Router;

public class FragmentProfile extends BaseFragment implements FragmentProfileMvpView, BackButtonListener {

    @Inject
    Toaster toaster;
    @Inject
    Router router;
    @BindView(R.id.single_image_full)
    ImageView imageView;
    @BindView(R.id.avatar)
    RoundedImageView avatar;
    MainActivity activity;
    @InjectPresenter
    FragmentProfileMvpPresenter presenter;
    @BindView(R.id.name)
    AppCompatTextView textView;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    LogoutController logoutController;

    AlertDialog.Builder ad;

    public FragmentProfile() {
        super(true, false, Orientation.NONE);
    }

    public static FragmentProfile getNewInstance() {
        FragmentProfile fragment = new FragmentProfile();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }


    @ProvidePresenter
    public FragmentProfileMvpPresenter createPresenter() {
        return new FragmentProfileMvpPresenter(router);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        activity.showBottomNavigationView();
        activity.getSupportActionBar().setTitle(R.string.profile);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.profile_toolbar, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_logout)
            ad = new AlertDialog.Builder(activity);
        ad.setTitle("Выйти?");
        ad.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                logoutController.unauthorized();
                dialog.cancel();
            }
        });
        ad.setNegativeButton("Нет", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.cancel();
            }
        });
        ad.show();

        return true;
    }

    @Override
    public int onInflateLayout() {
        return R.layout.fragment_profile;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public boolean onBackPressed() {
        if (presenter != null)
            return presenter.onBackPressed();

        return false;
    }


    @Override
    public void onDataLoaded(List<VkUserModel> model) {

        ImageLoader.getInstance().displayImage(model.get(0).getPhoto(), imageView, UilHelper.getOptions()
                .resetViewBeforeLoading(true)
                .build());
        ImageLoader.getInstance().displayImage(model.get(0).getPhoto(), avatar, UilHelper.getOptions()
                .resetViewBeforeLoading(true)
                .build());
        textView.setText(model.get(0).getFirstName() + " " + model.get(0).getLastName());
    }

    @Override
    public void onDataError(String message) {
        if (message != null)
            router.showSystemMessage(message);
    }

    @Override
    public void unknownException() {
        if (router != null) router.exitWithMessage(getString(R.string.error_unknown));
    }

    @Override
    public void apiException(String message) {
        if (message != null && message.length() > 0) {
            if (router != null) router.exitWithMessage(message);
        } else unknownException();
    }

    @Override
    public void connectionException() {
        if (router != null) router.exitWithMessage(getString(R.string.error_connection));
    }

}
