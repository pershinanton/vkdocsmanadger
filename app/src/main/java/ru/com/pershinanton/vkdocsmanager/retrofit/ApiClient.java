package ru.com.pershinanton.vkdocsmanager.retrofit;

import java.util.List;

import retrofit2.http.POST;
import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;
import ru.com.pershinanton.vkdocsmanager.model.VkUserModel;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsDeleteResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsGetResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsGetUploadUrlResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsSaveResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkFriendsGetResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkSendMessageResponse;


public interface ApiClient {

    String BASE_HOST = "api.vk.com";
    String BASE_URL = "https://" + BASE_HOST + "/method/";

    String ACCESS_TOKEN = "access_token";
    String API_VERSION = "5.92";
    String V = "v";
    String USER_ID = "user_id";
    String DOC_ID = "doc_id";
    String OWNER_ID = "owner_id";
    String FIELDS = "fields";
    String LANG = "lang";
    String RU = "ru";
    String OFFSET = "offset";
    String COUNT = "count";
    String TYPE = "type";
    String FILE = "file";
    String TITLE = "title";
    String ORDER = "order";
    String NAME_CASE = "name_case";
    String ATTACMENT = "attachment";
    String Q = "q";

    int ITEMS_PER_PAGE = 20;



    @Headers("Content-Type: application/json")
    @GET("docs.get")
    Call<VkResponse<VkDocsGetResponse>> docsGet(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token, @Query(COUNT) int count, @Query(OFFSET) int offset, @Query(TYPE) int type, @Query(OWNER_ID) String ownerID);

    @Headers("Content-Type: application/json")
    @GET("docs.getUploadServer")
    Call<VkResponse<VkDocsGetUploadUrlResponse>> docsGetUploadServer(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token);

    @Headers("Content-Type: application/json")
    @GET("docs.save")
    Call<VkResponse<VkDocsSaveResponse>> docSave(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token,@Query(FILE) String file, @Query(TITLE) String title);

    @Headers("Content-Type: application/json")
    @GET("docs.delete")
    Call<VkDocsDeleteResponse> docsDelete(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token, @Query(OWNER_ID) String ownerId, @Query(DOC_ID) long docId);

    @Headers("Content-Type: application/json")
    @GET("users.get")
    Call<VkResponse<List<VkUserModel>>> usersGet(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token, @Query(FIELDS) String photo);

//    @Headers("Content-Type: application/json")
//    @GET("friends.get")
//    Call<VkResponse<VkFriendsGetResponse>> friendsGet(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token, @Query(USER_ID) String userId, @Query(ORDER) String order,@Query(COUNT) int count,@Query(OFFSET) int offset, @Query(FIELDS) String fields, @Query(NAME_CASE) String nameCase);
//
//    @Headers("Content-Type: application/json")
//    @GET("friends.search")
//    Call<VkResponse<VkFriendsGetResponse>> friendsSearch(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token, @Query(USER_ID) String userId, @Query(Q) String q, @Query(FIELDS) String fields , @Query(NAME_CASE) String nameCase, @Query(OFFSET) int offset, @Query(COUNT) int count);
//
//    @Headers("Content-Type: application/json")
//    @GET("message.send")
//    Call<VkSendMessageResponse> messageSend(@Query(V) String v, @Query(LANG) String lang, @Query(ACCESS_TOKEN) String token, @Query(USER_ID) int userId, @Query(ATTACMENT) String attachment);

}
