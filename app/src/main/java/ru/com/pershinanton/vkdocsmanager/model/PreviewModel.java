package ru.com.pershinanton.vkdocsmanager.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreviewModel {


    @SerializedName("photo")
    @Expose
    private PhotoModel photoModel;

    public PhotoModel getPhotoModel() {
        return photoModel;
    }
}