package ru.com.pershinanton.vkdocsmanager.main;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.hwangjr.rxbus.annotation.Subscribe;
import com.hwangjr.rxbus.annotation.Tag;
import com.hwangjr.rxbus.thread.EventThread;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.BusDownload;
import ru.com.pershinanton.vkdocsmanager.LogoutController;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.base.UnauthorizedIOException;
import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.retrofit.ApiClient;
import ru.com.pershinanton.vkdocsmanager.retrofit.DownloadFileApiClient;
import ru.com.pershinanton.vkdocsmanager.retrofit.UploadFileApiClient;
import ru.com.pershinanton.vkdocsmanager.retrofit.exception.ApiException;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsDeleteResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsGetResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsGetUploadUrlResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsSaveResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsUploadFileResponse;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkResponse;
import ru.com.pershinanton.vkdocsmanager.rxjava.RetryWithDelay;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;

import static com.google.android.exoplayer2.ExoPlayerLibraryInfo.TAG;

@InjectViewState
public class FragmentWallMvpPresenter extends BaseNetworkMvpPresenter<FragmentWallMvpView> implements BackButtonListener, RetryWithDelay.Listener {// в этом классе мы получаем данные из других мест
    @Inject
    Toaster toaster;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    ApiClient apiClient;
    @Inject
    UploadFileApiClient uploadFileApiClient;
    @Inject
    DownloadFileApiClient downloadFileApiClient;
    @Inject
    LogoutController logoutController;
    private Router router;
    private Disposable disposable;
    private Disposable disposableUpload;

    @Subscribe(
            thread = EventThread.MAIN_THREAD,
            tags = {
                    @Tag(BusDownload.DOWNLOAD_PERCENT)
            }
    )
    public void downloading(Integer percent) {
        getViewState().onDataLoading(percent);
    }

    @Subscribe(
            thread = EventThread.MAIN_THREAD,
            tags = {
                    @Tag(BusDownload.DOWNLOAD_ERROR)
            }
    )
    public void eror(boolean error) {
        getViewState().onLoadingError("Ошибка загрузки");
    }

    public FragmentWallMvpPresenter(Router router) {
        super(false);
        this.router = router;
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        load(0);
    }

    public void load(int offset) {
        disposable = Flowable.just(true)
//                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
//                    .retry(throwable -> onRequestRetryNetwork(throwable))
//                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {
                    Response<VkResponse<VkDocsGetResponse>> response;
                    try {                                           // response ответ с сервера по обращению
                        response = apiClient.docsGet(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken(), 10, offset, 0, authPreferences.getUserId()).execute();
//                      // https://api.vk.com/method/docs.get?v=5.92&lang=ru&access_token=ab22e38f8aa3a11cc87f7c49b8f49a683019f6fced677fc2588061e55db2833cb2739f338da3079ac9f0d&count=5&offset=0&type=0&owner_id=4212530
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new ApiException();
                    if (response.code() != 200) throw new ApiException();
                    if (response.errorBody() != null) throw new ApiException();
                    if (response.body() == null) throw new ApiException();
                    VkResponse<VkDocsGetResponse> apiResponse = response.body();

                    if (apiResponse.getError() != null && apiResponse.getError().getErrorCode() > 0) {
                        if (apiResponse.getError().getErrorCode() == 5) {
                            logoutController.unauthorized();
                            throw new UnauthorizedIOException();
                        } else {
                            if (apiResponse != null && apiResponse.getError().getErrorMsg() != null && apiResponse.getError().getErrorMsg().length() > 0)
                                throw new ApiException(apiResponse.getError().getErrorMsg());
                            else
                                throw new ApiException();
                        }
                    }

                    VkDocsGetResponse result = apiResponse.response;

                    if (result == null)
                        throw new ApiException();
                    List<VkDocsModel> items = result.getItems();
                    if (items == null) throw new ApiException();

                    return items;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    getViewState().onDataLoaded(list);
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    public void firstUpload(String filePath, String title) {
        disposeUpload();
        disposableUpload = Flowable.just(true)
//                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
//                    .retry(throwable -> onRequestRetryNetwork(throwable))
//                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {
                    Response<VkResponse<VkDocsGetUploadUrlResponse>> response;
                    try {                                           // response ответ с сервера по обращению
                        response = apiClient.docsGetUploadServer(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken()).execute();
//
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new ApiException();
                    if (response.code() != 200) throw new ApiException();
                    if (response.errorBody() != null) throw new ApiException();
                    if (response.body() == null) throw new ApiException();
                    VkResponse<VkDocsGetUploadUrlResponse> apiResponse = response.body();

                    if (apiResponse.getError() != null && apiResponse.getError().getErrorCode() > 0) {
                        if (apiResponse.getError().getErrorCode() == 5) {
                            logoutController.unauthorized();
                            throw new UnauthorizedIOException();
                        } else {
                            if (apiResponse != null && apiResponse.getError().getErrorMsg() != null && apiResponse.getError().getErrorMsg().length() > 0)
                                throw new ApiException(apiResponse.getError().getErrorMsg());
                            else
                                throw new ApiException();
                        }
                    }

                    VkDocsGetUploadUrlResponse result = apiResponse.response;

                    if (result == null)
                        throw new ApiException();
                    String url = result.getUploadUrl();
                    if (url == null) throw new ApiException();

                    return url;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(url -> {
                    upload(url, filePath, title);
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    private void upload(String url, String filePath, String title) {
        disposeUpload();
        disposableUpload = Flowable.just(true)
//                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
//                    .retry(throwable -> onRequestRetryNetwork(throwable))
//                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {

                    File file = new File(filePath);
                    RequestBody requestFile =
                            RequestBody.create(
                                    MediaType.parse("multipart/form-data"),
                                    file
                            );
                    MultipartBody.Part body =
                            MultipartBody.Part.createFormData("file", file.getName(), requestFile);


                    Response<VkDocsUploadFileResponse> response;
                    try {
                        response = uploadFileApiClient.uploadFile(url, body).execute();
//
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new ApiException();
                    if (response.code() != 200) throw new ApiException();
                    if (response.errorBody() != null) throw new ApiException();
                    if (response.body() == null) throw new ApiException();
                    VkDocsUploadFileResponse result = response.body();
                    if (result.getError() != null)
                        throw new ApiException(result.getError());
                    return result;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(vkDocsUploadFileResponse -> {
                    save(vkDocsUploadFileResponse.getFile(), title);
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    public void save(String file, String title) {
        disposeUpload();
        disposableUpload = Flowable.just(true)
//                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
//                    .retry(throwable -> onRequestRetryNetwork(throwable))
//                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {
                    Response<VkResponse<VkDocsSaveResponse>> response;
                    try {                                           // response ответ с сервера по обращению
                        response = apiClient.docSave(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken(), file, title).execute();
//                      // https://api.vk.com/method/docs.get?v=5.92&lang=ru&access_token=ab22e38f8aa3a11cc87f7c49b8f49a683019f6fced677fc2588061e55db2833cb2739f338da3079ac9f0d&count=5&offset=0&type=0&owner_id=4212530
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new ApiException();
                    if (response.code() != 200) throw new ApiException();
                    if (response.errorBody() != null) throw new ApiException();
                    if (response.body() == null) throw new ApiException();
                    VkResponse<VkDocsSaveResponse> apiResponse = response.body();

                    if (apiResponse.getError() != null && apiResponse.getError().getErrorCode() > 0) {
                        if (apiResponse.getError().getErrorCode() == 5) {
                            logoutController.unauthorized();
                            throw new UnauthorizedIOException();
                        } else {
                            if (apiResponse != null && apiResponse.getError().getErrorMsg() != null && apiResponse.getError().getErrorMsg().length() > 0)
                                throw new ApiException(apiResponse.getError().getErrorMsg());
                            else
                                throw new ApiException();
                        }
                    }

                    VkDocsSaveResponse result = apiResponse.response;

                    if (result == null)
                        throw new ApiException();

                    return result;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(vkDocsSaveResponse -> {
                    getViewState().onDataLoaded(vkDocsSaveResponse);
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    public void delete(long docId) {
        disposable = Flowable.just(true)
//                .delay(2, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.newThread())
//                    .retry(throwable -> onRequestRetryNetwork(throwable))
//                    .retryWhen(new RetryWithDelay(RetryWithDelay.UNLIMITED, 3000, this))
                .map((Boolean value) -> {
                    Response<VkDocsDeleteResponse> response;
                    try {                                           // response ответ с сервера по обращению
                        response = apiClient.docsDelete(ApiClient.API_VERSION, ApiClient.RU, authPreferences.getAccessToken(), authPreferences.getUserId(), docId).execute();
//
                    } catch (Throwable throwable) {
                        throw new Exception(throwable);
                    }
                    if (response == null) throw new ApiException();
                    if (response.code() != 200) throw new ApiException();
                    if (response.errorBody() != null) throw new ApiException();
                    if (response.body() == null) throw new ApiException();

                    VkDocsDeleteResponse result = response.body();

                    if (result == null)
                        throw new ApiException();
                    return result;

                }).observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                }, throwable -> {
                    onThrowable(throwable);
                });
    }

    public void download(String fileUrl, String title, String filePath) {

        Call<ResponseBody> call = downloadFileApiClient.downloadFileWithDynamicUrlSync(fileUrl);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "server contacted and has file");

                    boolean writtenToDisk = writeResponseBodyToDisk(response.body(), title, fileUrl, filePath);
                    if (writtenToDisk == true) {
                        getViewState().onLoadingFinished("Загрузка " + title + " завершена");
                    }
                    Log.d(TAG, "file download was a success? " + writtenToDisk);
                } else {
                    Log.d(TAG, "server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "error");
            }
        });
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String title, String fileUrl, String filePath) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(filePath + File.separator + title);
            if (futureStudioIconFile.exists())
                download(fileUrl, title.length() - 4 + " copy", filePath);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();

                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    protected void dispose() {
        if (disposable != null) {
            if (!disposable.isDisposed()) {
                disposable.dispose();
            }
            disposable = null;
        }
    }

    protected void disposeUpload() { //останавливает асинхронность
        if (disposableUpload != null) {
            if (!disposableUpload.isDisposed()) {
                disposableUpload.dispose();
            }
            disposableUpload = null;
        }
    }

    @Override
    public void onDestroy() {
        dispose();
        disposeUpload();
        super.onDestroy();
        router = null;
    }

    @Override
    public boolean onBackPressed() {
        if (router != null) router.exit();
        return true;
    }

}