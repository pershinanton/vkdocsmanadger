package ru.com.pershinanton.vkdocsmanager.main;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import ru.com.pershinanton.vkdocsmanager.main.base.BaseNetworkMvpView;
import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;
import ru.com.pershinanton.vkdocsmanager.retrofit.model.VkDocsSaveResponse;

import java.util.List;

public interface FragmentWallMvpView extends BaseNetworkMvpView {
    @StateStrategyType(AddToEndStrategy.class)
    void onDataLoaded(List<VkDocsModel> list);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataError(String message);

    @StateStrategyType(AddToEndStrategy.class)
    void onDataLoaded(VkDocsSaveResponse vkDocsSaveResponse);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onDataLoading(Integer percent);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onLoadingFinished(String finished);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void onLoadingError(String error);

}
