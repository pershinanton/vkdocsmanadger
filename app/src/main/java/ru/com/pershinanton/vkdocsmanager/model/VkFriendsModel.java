package ru.com.pershinanton.vkdocsmanager.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VkFriendsModel {

    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("photo_200_orig")
    private String photo;
    @SerializedName("online")
    private int online;
    @SerializedName("id")
    private int friendId;

    public int getFriendId() {
        return friendId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoto() {
        return photo;
    }

    public int getOnline() {
        return online;
    }
}