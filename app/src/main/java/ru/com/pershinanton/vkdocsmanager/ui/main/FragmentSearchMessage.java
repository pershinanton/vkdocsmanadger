//package ru.com.pershinanton.vkdocsmanager.ui.main;
//
//import android.content.Context;
//import android.os.Bundle;
//import android.os.Parcel;
//import android.os.Parcelable;
//import android.support.annotation.Nullable;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.widget.EditText;
//
//import com.arellomobile.mvp.presenter.InjectPresenter;
//import com.arellomobile.mvp.presenter.ProvidePresenter;
//import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
//
//import java.util.List;
//
//import javax.inject.Inject;
//
//import butterknife.BindView;
//import ru.com.pershinanton.vkdocsmanager.App;
//import ru.com.pershinanton.vkdocsmanager.R;
//import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
//import ru.com.pershinanton.vkdocsmanager.main.FragmentSearchMvpPresenter;
//import ru.com.pershinanton.vkdocsmanager.main.FragmentSearchMvpView;
//import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;
//import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
//import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.VkFriendsWallPaginationAdapter;
//import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.utils.RequestLoadNextController;
//import ru.com.pershinanton.vkdocsmanager.ui.base.BaseFragment;
//import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
//import ru.terrakok.cicerone.Router;
//
//public class FragmentSearchMessage extends BaseFragment implements FragmentSearchMvpView, BackButtonListener {
//
//    @Inject
//    Toaster toaster;
//    @Inject
//    Router router;
//    @BindView(R.id.recyclerView)
//    RecyclerView recyclerView;
//    @BindView(R.id.editText)
//    EditText editText;
//    @InjectPresenter
//    FragmentSearchMvpPresenter presenter;
//    @Inject
//    AuthPreferences authPreferences;
//
//    MainActivity activity;
//    private VkFriendsWallPaginationAdapter adapter;
//
//    public FragmentSearchMessage() {
//        super(true, false, Orientation.NONE);
//        setHasOptionsMenu(true);
//    }
//
//    public static FragmentSearchMessage getNewInstance(InstantiateObject instantiateObject) {
//        FragmentSearchMessage fragment = new FragmentSearchMessage();
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(INSTANTIATE_OBJECT, instantiateObject);
//        fragment.setArguments(bundle);
//        return fragment;
//    }
//
//    @ProvidePresenter
//    public FragmentSearchMvpPresenter createPresenter() {
//        return new FragmentSearchMvpPresenter(router);
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        App.INSTANCE.getAppComponent().inject(this);
//        super.onCreate(savedInstanceState);
//    }
//
//
//    @Override
//    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
//        activity.showBottomNavigationView();
//        activity.getSupportActionBar().setTitle("Отправить другу");
//        activity.getSupportActionBar().show();
//        initRecyclerView();
//        editText.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                presenter.loadText(s.toString());
//                try {
//                    Thread.sleep(50);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                adapter.notifyDataSetChanged();
//            }
//        });
//    }
//
//    private void initRecyclerView() {
//        recyclerView.addItemDecoration(
//                new HorizontalDividerItemDecoration.Builder(activity)
//                        .build());
//        recyclerView.setHasFixedSize(true);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
//        recyclerView.setLayoutManager(layoutManager);
//        adapter = new VkFriendsWallPaginationAdapter(activity, new VkFriendsWallPaginationAdapter.Listener() {
//            @Override
//            public void onClickFriend(VkFriendsModel vkFriendsModel) {
//                presenter.send(vkFriendsModel.getFriendId(), "<doc><" + authPreferences.getUserId() + "><" + getMessageId() + ">");
//            }
//        }).setRequestLoadNextListener(new RequestLoadNextController.OnRequestLoadNextListener() {
//            @Override
//            public void onRequestLoad() {
//            }
//        });
//        recyclerView.setAdapter(adapter);
//    }
//
//    @Override
//    public int onInflateLayout() {
//        return R.layout.fragment_search;
//    }
//
//    @Override
//    public boolean onBackPressed() {
//        if (presenter != null) return presenter.onBackPressed();
//        return true;
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        activity = (MainActivity) getActivity();
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        activity = null;
//    }
//
//    @Override
//    public void onDataLoaded(List<VkFriendsModel> list) {
//        if (adapter != null)
//            adapter.setModels(list, false);
//
//    }
//
//    @Override
//    public void onDataError(String message) {
//        if (message != null)
//            router.showSystemMessage(message);
//    }
//
//
//    @Override
//    public void unknownException() {
//        if (router != null) router.exitWithMessage(getString(R.string.error_unknown));
//    }
//
//    @Override
//    public void apiException(String message) {
//        if (message != null && message.length() > 0) {
//            if (router != null) router.exitWithMessage(message);
//        } else unknownException();
//    }
//
//    @Override
//    public void connectionException() {
//        if (router != null) router.exitWithMessage(getString(R.string.error_connection));
//    }
//
//
//    private int getMessageId() {
//        return getIntantiataeObject().getMessageId();
//    }
//
//    private FragmentSearchMessage.InstantiateObject getIntantiataeObject() {
//        return getArguments().getParcelable(INSTANTIATE_OBJECT);
//    }
//
//    public static class InstantiateObject implements Parcelable {
//        int messageId;
//
//        public int getMessageId() {
//            return messageId;
//        }
//
//        public InstantiateObject(int messageId) {
//            this.messageId = messageId;
//        }
//
//        protected InstantiateObject(Parcel in) {
//            this.messageId = in.readInt();
//        }
//
//        public static final Creator<InstantiateObject> CREATOR = new Creator<InstantiateObject>() {
//            @Override
//            public InstantiateObject createFromParcel(Parcel in) {
//                return new InstantiateObject(in);
//            }
//
//            @Override
//            public InstantiateObject[] newArray(int size) {
//                return new InstantiateObject[size];
//            }
//        };
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel dest, int flags) {
//        }
//    }
//
//}
