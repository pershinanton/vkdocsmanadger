//package ru.com.pershinanton.vkdocsmanager.preferences;
//
//import android.app.Application;
//import android.content.Context;
//import android.content.SharedPreferences;
//
//import java.util.UUID;
//
//import javax.inject.Inject;
//import javax.inject.Singleton;
//
//@Singleton
//public class UidPreferences {
//
//    public static final String UID = "ACCESS_TOKEN";
//    private static final String PREFS_FILENAME = UidPreferences.class.getSimpleName();
//
//    private final SharedPreferences prefs;
//    private String uid;
//
//    @Inject
//    public UidPreferences(Application application) {
//        prefs = application.getSharedPreferences(PREFS_FILENAME,
//                Context.MODE_PRIVATE);
//    }
//
//    public String getUid() {
//        if (uid == null) {
//            uid = prefs.getString(UID, null);
//            if (uid == null)
//                setUid(UUID.randomUUID().toString());
//        }
//        return uid;
//    }
//
//    public void setUid(String uid) {
//        this.uid = uid;
//        SharedPreferences.Editor edit = prefs.edit();
//        edit.putString(UID, uid);
//        edit.commit();
//    }
//
//}