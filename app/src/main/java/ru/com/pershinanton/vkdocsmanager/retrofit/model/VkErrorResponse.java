package ru.com.pershinanton.vkdocsmanager.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VkErrorResponse {

    @SerializedName("error_code")
    @Expose
    public int errorCode;
    @SerializedName("error_msg")
    @Expose
    public String errorMsg;

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }
}