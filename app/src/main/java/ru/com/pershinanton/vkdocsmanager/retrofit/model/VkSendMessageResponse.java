package ru.com.pershinanton.vkdocsmanager.retrofit.model;


import com.google.gson.annotations.SerializedName;

import ru.com.pershinanton.vkdocsmanager.model.VkMessageModel;


public class VkSendMessageResponse {
    public VkMessageModel response;
    @SerializedName("peer_id")
    private String peerId;
    @SerializedName("message_id")
    private String messageId;
    @SerializedName("error")
    private String error;

    public String getPeerId() {
        return peerId;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getError() {
        return error;
    }
}
