package ru.com.pershinanton.vkdocsmanager.lyra;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.com.pershinanton.vkdocsmanager.App;
import com.fondesa.lyra.coder.StateCoder;
import com.google.gson.Gson;

import java.util.HashMap;

import javax.inject.Inject;


public class GsonHashMapStateCoder implements StateCoder<HashMap> {

    @Inject
    Gson gson;

    public GsonHashMapStateCoder() {
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    public void serialize(@NonNull Bundle state, @NonNull String key, @NonNull HashMap fieldValue) {
        try {
            String jsonString = gson.toJson(fieldValue);
            if (jsonString != null)
                state.putString(key, jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public HashMap deserialize(@NonNull Bundle state, @NonNull String key) {
        try {
            String jsonString = state.getString(key);
            if (jsonString != null)
                return gson.fromJson(jsonString, HashMap.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}