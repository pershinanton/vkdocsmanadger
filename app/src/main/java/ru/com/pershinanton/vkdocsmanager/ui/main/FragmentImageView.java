package ru.com.pershinanton.vkdocsmanager.ui.main;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;

import com.alexvasilkov.gestures.views.GestureImageView;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.dinuscxj.progressbar.CircleProgressBar;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.hwangjr.rxbus.annotation.Tag;
import com.pnikosis.materialishprogress.ProgressWheel;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.Screens;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.main.FragmentImageViewMvpPresenter;
import ru.com.pershinanton.vkdocsmanager.main.FragmentImageViewMvpView;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.ui.base.BaseFragment;
import ru.com.pershinanton.vkdocsmanager.ui.common.BackButtonListener;
import ru.terrakok.cicerone.Router;


public class FragmentImageView extends BaseFragment implements FragmentImageViewMvpView, BackButtonListener {

    @Inject
    Toaster toaster;
    @Inject
    Router router;
    @BindView(R.id.single_image_full)
    GestureImageView gestureView;
    @BindView(R.id.bottomDownload)
    Button button;
    @BindView(R.id.progressBar)
    CircleProgressBar progressBar;
    @BindView(R.id.full_screen_video)
    PlayerView mPlayerView;
    @BindView(R.id.progress_wheel)
    ProgressWheel progressWheel;
    @Inject
    AuthPreferences authPreferences;
    @InjectPresenter
    FragmentImageViewMvpPresenter presenter;
    MainActivity activity;
    private SimpleExoPlayer simpleExoPlayer;
    private long mCurrentMillis;



    public FragmentImageView() {
        super(true, true, Orientation.NONE);
    }


    public static FragmentImageView getNewInstance(InstantiateObject instantiateObject) {
        FragmentImageView fragment = new FragmentImageView();
        Bundle bundle = new Bundle();
        bundle.putParcelable(INSTANTIATE_OBJECT, instantiateObject);
        fragment.setArguments(bundle);
        return fragment;
    }

    @ProvidePresenter
    public FragmentImageViewMvpPresenter createPresenter() {
        return new FragmentImageViewMvpPresenter(router);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        activity.hideBottomNavigationView();
        activity.getSupportActionBar().setTitle("view");
        activity.getSupportActionBar().hide();
        progressBar.setVisibility(View.INVISIBLE);
        button.setVisibility(View.INVISIBLE);
        progressWheel.setVisibility(View.INVISIBLE);

        if (getType() == 3) {
            button.setVisibility(View.INVISIBLE);
            progressWheel.setVisibility(View.VISIBLE);
            progressWheel.spin();
            Glide.with(this)
                    .asGif()
                    .load(getUrl())
                    .into(new SimpleTarget<GifDrawable>() {
                        @Override
                        public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
                            progressWheel.setVisibility(View.INVISIBLE);
                            progressWheel.stopSpinning();
                            button.setVisibility(View.VISIBLE);
                            gestureView.setImageDrawable(resource);
                            resource.start();
                        }
                    });
        }

        if (getType() == 6){
            mPlayerView.setVisibility(View.VISIBLE);
            startPlayer();
        }
        if(getType()!= 3 && getType() !=6 ){
            progressWheel.setVisibility(View.VISIBLE);
            progressWheel.spin();
            Glide.with(this)
                    .asBitmap()
                    .load(getUrl())
                    .apply(new RequestOptions().override(1600, 1600))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            progressWheel.setVisibility(View.INVISIBLE);
                            button.setVisibility(View.VISIBLE);
                            gestureView.setImageBitmap(resource);
                        }
                    });
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(activity, v);
                popupMenu.inflate(R.menu.popup_menu_save);
                popupMenu
                        .setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.download:
                                        presenter.download(getUrl(), getTitle(), authPreferences.getFilePath());
                                        progressBar.setVisibility(View.VISIBLE);
                                        return true;
                                    default:
                                        return false;
                                }
                            }
                        });
                popupMenu.show();
            }
        });

    }

    private void startPlayer () {
//        progressWheel.setVisibility(View.INVISIBLE);
        if (simpleExoPlayer != null) {
            // no need to continue creating the player, if it's probably there
            return;
        }
        // set default options for the player
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(activity),
                new DefaultTrackSelector());
        mPlayerView.setPlayer(simpleExoPlayer);

        DefaultDataSourceFactory dataSourceFactory =
                new DefaultDataSourceFactory(activity, Util.getUserAgent(activity, "player"));
        ExtractorMediaSource extractorMediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(getUrl()));

        // now is the more important part. here we check to see if we want to resume, or start from the beggining
        boolean isResuming = mCurrentMillis != 0;
        simpleExoPlayer.prepare(extractorMediaSource, isResuming, false);
        simpleExoPlayer.setPlayWhenReady(true);
        if (isResuming) {
            // want to resume? seek to the old position
            simpleExoPlayer.seekTo(mCurrentMillis);
        }
    }

    private void release () {
        if (simpleExoPlayer == null) {
            return;
        }
        mCurrentMillis = simpleExoPlayer.getCurrentPosition();
        simpleExoPlayer.release();
        simpleExoPlayer = null;
    }
    @Override
    public int onInflateLayout() {
        return R.layout.fragment_full_size_image;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        startPlayer();


    }

    @Override
    public void onPause() {
        release();
        super.onPause();

    }

    @Override
    public boolean onBackPressed() {
        if (presenter != null)
            return presenter.onBackPressed();
        return false;
    }

    @Override
    public void onDataError(String message) {
        if (message != null)
            router.showSystemMessage(message);
    }

    @Override
    public void onDataLoading(Integer percent) {
        progressBar.setProgress(percent);
    }

    @Override
    public void onLoadingFinished(String finished) {
        progressBar.setVisibility(View.INVISIBLE);
        if (finished != null)
            router.showSystemMessage(finished);
    }

    @Override
    public void onLoadingError(String error) {
        if (error != null)
            router.showSystemMessage(error);
    }


    @Override
    public void unknownException() {
        if (router != null) router.exitWithMessage(getString(R.string.error_unknown));
    }

    @Override
    public void apiException(String message) {
        if (message != null && message.length() > 0) {
            if (router != null) router.exitWithMessage(message);
        } else unknownException();
    }

    @Override
    public void connectionException() {
        if (router != null) router.exitWithMessage(getString(R.string.error_connection));
    }

    private String getUrl() {
        return getIntantiataeObject().getUrl();
    }

    private String getTitle() {
        return getIntantiataeObject().getTitle();
    }

    private int getType() {
        return getIntantiataeObject().getType();
    }

    private InstantiateObject getIntantiataeObject() {
        return getArguments().getParcelable(INSTANTIATE_OBJECT);
    }

    public static class InstantiateObject implements Parcelable {


        public String getUrl() {
            return url;
        }

        public String getTitle() {
            return title;
        }

        public int getType() {
            return type;
        }

        private final String url;

        private final String title;

        private final int type;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.url);
        }

        public InstantiateObject(String url, String title, int type) {
            this.url = url;
            this.title = title;
            this.type = type;
        }

        protected InstantiateObject(Parcel in) {
            this.type = in.readInt();
            this.url = in.readString();
            this.title = in.readString();
        }

        public static final Creator<InstantiateObject> CREATOR = new Creator<InstantiateObject>() {
            @Override
            public InstantiateObject createFromParcel(Parcel source) {
                return new InstantiateObject(source);
            }

            @Override
            public InstantiateObject[] newArray(int size) {
                return new InstantiateObject[size];
            }
        };
    }

}
