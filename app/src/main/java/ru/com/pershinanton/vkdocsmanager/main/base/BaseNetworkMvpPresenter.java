package ru.com.pershinanton.vkdocsmanager.main.base;

import com.hwangjr.rxbus.RxBus;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpRetryException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;

import ru.com.pershinanton.vkdocsmanager.retrofit.exception.ApiException;

public class BaseNetworkMvpPresenter<View extends BaseNetworkMvpView> extends BaseMvpPresenter<View> implements BaseNetworkMvpView {

    public BaseNetworkMvpPresenter(boolean useBus) {
        if (useBus)
            RxBus.get().register(this);
    }

    @Override
    public void onDestroy() {
        try {
            RxBus.get().unregister(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public boolean onRequestRetryNetwork(Throwable throwable) {
        if (throwable != null) {
            if (throwable.getCause() instanceof ConnectException) return true;
            else if (throwable.getCause() instanceof UnknownHostException) return true;
            else if (throwable.getCause() instanceof SocketTimeoutException) return true;
            else if (throwable.getCause() instanceof UnknownServiceException) return true;
            else if (throwable.getCause() instanceof SocketException) return true;
            else if (throwable.getCause() instanceof HttpRetryException) return true;
            else return throwable.getCause() instanceof NoRouteToHostException;
        }
        return false;
    }

    public void onThrowable(Throwable throwable) {
        if (throwable != null) {
            if (throwable instanceof ApiException) {
                if (throwable.getMessage() != null)
                    getViewState().apiException(throwable.getMessage());
                else getViewState().unknownException();
                return;
            } else if (throwable instanceof UnauthorizedIOException) {
                return;
            } else if (throwable instanceof UnknownHostException) {
                getViewState().connectionException();
                return;
            } else if (throwable instanceof IOException) {
                getViewState().connectionException();
                return;
            } else if (throwable.getCause() instanceof ConnectException) {
                getViewState().connectionException();
                return;
            }
        }
        getViewState().unknownException();
    }

    @Override
    public void unknownException() {

    }

    @Override
    public void apiException(String message) {

    }

    @Override
    public void connectionException() {

    }
}