package ru.com.pershinanton.vkdocsmanager.ui.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;

import javax.inject.Inject;

import butterknife.BindView;
import ru.com.pershinanton.vkdocsmanager.App;
import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.preferences.AuthPreferences;
import ru.com.pershinanton.vkdocsmanager.ui.base.BaseFragment;
import ru.terrakok.cicerone.Router;

public class FragmentLogin extends BaseFragment {

    @BindView(R.id.enter)
    AppCompatTextView enter;
    @BindView(R.id.background)
    ImageView background;
    @Inject
    AuthPreferences authPreferences;
    @Inject
    Router router;
    MainActivity activity;
    @Inject
    Toaster toaster;

    public FragmentLogin() {
        super(true, false, Orientation.NONE);
    }

    public static FragmentLogin getNewInstance() {
        FragmentLogin fragment = new FragmentLogin();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        App.INSTANCE.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);


    }

    public static String uriFromDrawableResource(Integer resId) {
        return "drawable://" + resId;
    }

    public static String uriFromFilePatch(String patch) {
        return "file://" + patch;
    }

    @Override
    public void onViewInflated(View view, @Nullable Bundle savedInstanceState) {
        activity.getSupportActionBar().setTitle(R.string.login);
        activity.hideBottomNavigationView();
        Glide.with(this)
                .asBitmap()
                .load(R.drawable.docs_login)
                .apply(new RequestOptions().override(1600, 1600))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        background.setImageBitmap(resource);
                    }
                });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VKSdk.login(activity, VKScope.OFFLINE, VKScope.DOCS, VKScope.FRIENDS);
                authPreferences.setFilePath("/mnt/sdcard/download");
            }
        });
    }

    @Override
    public int onInflateLayout() {
        return R.layout.fragment_login;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        activity = (MainActivity) getActivity();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }

}
