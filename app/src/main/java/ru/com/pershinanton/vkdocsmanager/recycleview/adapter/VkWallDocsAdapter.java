package ru.com.pershinanton.vkdocsmanager.recycleview.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.model.VkDocsModel;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.viewholder.WallDocsViewHolder;

import java.util.ArrayList;
import java.util.List;

public class VkWallDocsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_PHOTO = 0;

    private final List<VkDocsModel> models = new ArrayList<>();
    private Listener listener;
    private LayoutInflater inflater;


    public VkWallDocsAdapter(Context context, List<VkDocsModel> models, Listener listener) {
        if (models != null)
            this.models.addAll(models);
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_PHOTO:
                return new WallDocsViewHolder(inflater.inflate(R.layout.vk_wall_item, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof WallDocsViewHolder) {
            WallDocsViewHolder vh = (WallDocsViewHolder) viewHolder;

        }
        viewHolder.itemView.setOnClickListener(v -> {
            if (listener != null)
                listener.onClickWall(models.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public int getItemViewType(int position) {

        return 0;
    }

    public interface Listener {
        void onClickWall(VkDocsModel vkDocsModel);
    }

}
