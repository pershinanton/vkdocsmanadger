package ru.com.pershinanton.vkdocsmanager.lyra;

import android.os.Bundle;
import android.support.annotation.NonNull;

import ru.com.pershinanton.vkdocsmanager.App;
import com.fondesa.lyra.coder.StateCoder;
import com.google.gson.Gson;

import javax.inject.Inject;


public class GsonObjectStateCoder implements StateCoder<Object> {

    @Inject
    Gson gson;

    public GsonObjectStateCoder() {
        App.INSTANCE.getAppComponent().inject(this);
    }

    @Override
    public void serialize(@NonNull Bundle state, @NonNull String key, @NonNull Object fieldValue) {
        try {
            String jsonString = gson.toJson(fieldValue);
            if (jsonString != null)
                state.putString(key, jsonString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object deserialize(@NonNull Bundle state, @NonNull String key) {
        try {
            String jsonString = state.getString(key);
            if (jsonString != null)
                return gson.fromJson(jsonString, Object.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}