package ru.com.pershinanton.vkdocsmanager.recycleview.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.com.pershinanton.vkdocsmanager.R;
import ru.com.pershinanton.vkdocsmanager.dagger.Toaster;
import ru.com.pershinanton.vkdocsmanager.model.VkFriendsModel;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.utils.RequestLoadNextController;
import ru.com.pershinanton.vkdocsmanager.recycleview.adapter.viewholder.FriendsWallViewHolder;
import ru.com.pershinanton.vkdocsmanager.retrofit.ApiClient;
import ru.com.pershinanton.vkdocsmanager.utils.StartSnapHelper;
import ru.com.pershinanton.vkdocsmanager.utils.UilHelper;

import static android.support.v7.widget.LinearLayoutManager.HORIZONTAL;


public class VkFriendsWallPaginationAdapter extends RecyclerView.Adapter<FriendsWallViewHolder> {

    @Inject
    Toaster toaster;

    private final List<VkFriendsModel> wallModels = new ArrayList<>();

    private final RequestLoadNextController needLoadNextController = new RequestLoadNextController();
    private Listener listener;
    private LayoutInflater inflater;
    private Context context;


    public VkFriendsWallPaginationAdapter(Context context, Listener listener) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public FriendsWallViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = inflater.inflate(R.layout.vk_fiends_item, parent, false);
        return new FriendsWallViewHolder(view);
    }


    @Override
    public void onBindViewHolder(FriendsWallViewHolder viewHolder, final int position) {
        needLoadNextController.checkIsNeedLoadNext(getItemCount(), position);
        new StartSnapHelper().attachToRecyclerView(viewHolder.friendsRecyclerView);
        initRecyclerView(viewHolder, position);
    }


    private void initRecyclerView(FriendsWallViewHolder viewHolder, int position) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, HORIZONTAL, false);
        viewHolder.friendsRecyclerView.setLayoutManager(layoutManager);

        viewHolder.imageView.setImageDrawable(null);
        VkFriendsModel vkFriendsModel = wallModels.get(position);
        viewHolder.firstName.setText(wallModels.get(position).getFirstName());
        viewHolder.secondName.setText(wallModels.get(position).getLastName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onClickFriend(wallModels.get(position));
            }
        });
        Glide.with(context)
                .load(vkFriendsModel.getPhoto())
                .into( viewHolder.imageView);


        viewHolder.friendsRecyclerView.setAdapter(viewHolder.vkFriendsWallAdapter);
    }

    @Override
    public int getItemCount() {
        return wallModels.size();
    }


    public void setModels(List<VkFriendsModel> models, boolean needLoadNextControllerEnabled) {
        this.wallModels.clear();
        if (models != null)
            this.wallModels.addAll(models);
        needLoadNextController.resetNeedLoadNextCounter();
        needLoadNextController.setEnabled(needLoadNextControllerEnabled);
        notifyDataSetChanged();
    }

    public void addModels(List<VkFriendsModel> models) {
        int oldSize = this.wallModels.size();
        if (models != null)
            this.wallModels.addAll(models);
        notifyItemRangeInserted(oldSize, models.size());
    }

    public void resetNeedLoadNextCounter() {
        needLoadNextController.resetNeedLoadNextCounter();
    }

    public VkFriendsWallPaginationAdapter setRequestLoadNextListener(RequestLoadNextController.OnRequestLoadNextListener onRequestLoadNextListener) {
        needLoadNextController.setRequestLoadNextListener(ApiClient.ITEMS_PER_PAGE / 2, onRequestLoadNextListener);
        return this;
    }

    public interface Listener {
        void onClickFriend(VkFriendsModel vkFriendsModel);

    }

}
