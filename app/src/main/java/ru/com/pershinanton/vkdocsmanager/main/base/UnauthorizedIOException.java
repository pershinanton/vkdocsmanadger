package ru.com.pershinanton.vkdocsmanager.main.base;

import java.io.IOException;

public class UnauthorizedIOException extends IOException {
    public UnauthorizedIOException() {
        super();
    }
}
